package cz.filipobornik.testproject.ui.editUserAccountActivity

import android.net.Uri
import cz.filipobornik.testproject.base.BaseMVPPresenter
import cz.filipobornik.testproject.base.BaseMVPView
import cz.filipobornik.testproject.data.model.User
import java.io.File

abstract class EditUserAccountActivityContract {

    interface View : BaseMVPView {

        fun goBackToMainActivity()

        fun startPhotoPickerActivity()

        fun startCameraActivity()

        fun startUCropActivity(imageUri: Uri)

        fun showLoading()

        fun hideLoading()

        fun showBottomSheetDialogCameraOrGallery()

        fun onTakePictureBtnClicked()

        fun onSaveUserFabClicked()
    }

    interface Presenter : BaseMVPPresenter<View> {

        fun saveUser(user: User, selectedUserImageFile: File?)

    }

}