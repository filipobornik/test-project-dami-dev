package cz.filipobornik.testproject.ui.addContactActivity

import android.support.v7.app.AppCompatActivity
import cz.filipobornik.testproject.base.BaseActivityModule
import cz.filipobornik.testproject.di.PerActivity
import dagger.Binds
import dagger.Module

@Module(includes = [BaseActivityModule::class])
abstract class AddContactActivityModule {


    @Binds
    @PerActivity
    abstract fun provideActivity(contactActivity: AddContactActivity): AppCompatActivity

    @Binds
    @PerActivity
    abstract fun providePresenter(presenterContact: AddContactActivityPresenter): AddContactActivityContract.Presenter

}