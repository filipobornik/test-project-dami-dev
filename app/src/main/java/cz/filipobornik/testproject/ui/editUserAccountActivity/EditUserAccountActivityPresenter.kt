package cz.filipobornik.testproject.ui.editUserAccountActivity

import android.content.res.Resources
import cz.filipobornik.testproject.R
import cz.filipobornik.testproject.base.BasePresenter
import cz.filipobornik.testproject.data.DataManager
import cz.filipobornik.testproject.data.model.User
import cz.filipobornik.testproject.data.remote.NetworkMonitor
import cz.filipobornik.testproject.di.PerActivity
import cz.filipobornik.testproject.utils.ErrorUtils
import cz.filipobornik.testproject.utils.UserSessionManager
import java.io.File
import javax.inject.Inject

@PerActivity
class EditUserAccountActivityPresenter @Inject constructor(
        private val dataManager: DataManager,
        private val resourceManager: Resources,
        private val errorUtils: ErrorUtils,
        private val userSessionManager: UserSessionManager,
        private val networkMonitor: NetworkMonitor
): BasePresenter<EditUserAccountActivityContract.View>(), EditUserAccountActivityContract.Presenter {

    /**
     * Save user, on success notify view to go back to previous activity, on error notify view to show error
     */
    override fun saveUser(user: User, selectedUserImageFile: File?) {
        view?.showLoading()

        if (!networkMonitor.isConnected) {
            view?.hideLoading()
            view?.showError(R.string.no_network_connection)
            return
        }

        dataManager.updateAccount(user, selectedUserImageFile)
               .subscribe({ response ->
                    run {
                        if (response.response != null) {
                            userSessionManager.updateUser(user)
                            view?.showMessage(resourceManager.getString(R.string.user_account_updated))
                            view?.goBackToMainActivity()
                        }
                    }
                }, { error ->
                    run {
                        view?.hideLoading()
                        view?.showError(errorUtils.returnApiErrorMessage(error))
                    }
                })
    }
}