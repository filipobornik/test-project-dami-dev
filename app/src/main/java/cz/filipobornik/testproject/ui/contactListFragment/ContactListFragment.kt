package cz.filipobornik.testproject.ui.contactListFragment


import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import android.view.*
import android.view.animation.AnimationUtils
import cz.filipobornik.testproject.R
import cz.filipobornik.testproject.base.BaseFragment
import cz.filipobornik.testproject.data.model.Contact
import cz.filipobornik.testproject.ui.contactDetailActivity.ContactDetailActivity
import cz.filipobornik.testproject.utils.Constants
import kotlinx.android.synthetic.main.fragment_contact_list.*
import java.util.*
import javax.inject.Inject
import android.animation.LayoutTransition
import android.widget.LinearLayout


/**
 * Fragment displays list of contacts and search for the list
 */
class ContactListFragment : BaseFragment(), ContactListFragmentContract.View {

    @Inject
    lateinit var presenter: ContactListFragmentContract.Presenter

    @Inject
    lateinit var contactAdapter: ContactAdapter

    /**
     * Search view that enables user to search in contacts
     */
    private lateinit var searchView: SearchView

    /**
     * Timer that delays search in contacts
     */
    private var timer: Timer? = null

    /**
     * Snackbar for showing no internet connection error
     */
    private var noInternetConnectionSnackbar: Snackbar? = null

    /**
     * Shows contacts by updating contact adapter, filter contacts if search has query
     */
    override fun showContacts(contacts: ArrayList<Contact>) {
        if (contacts.size == 0) {
            showEmptyView()
        } else {
            hideEmptyView()
        }
        contactAdapter.updateContacts(contacts)
        contactAdapter.searchContacts(if (::searchView.isInitialized) {
            searchView.query.toString()
        } else "")
        recycler_view_contacts.scheduleLayoutAnimation()
    }

    /**
     * Handles contact click
     */
    override fun onContactClicked(contact: Contact) {
        startContactDetailActivity(contact)
    }

    /**
     * Starts contact detail activity for specific contact, put user values to intent's extras
     */
    override fun startContactDetailActivity(contact: Contact) {
        val intent = Intent(context, ContactDetailActivity::class.java)
        intent.putExtra(Constants.EXTRA_CONTACT, contact)
        startActivity(intent)
    }

    /**
     * Shows loading bar
     */
    override fun showLoading() {
        noInternetConnectionSnackbar?.dismiss()
        swipe_refresh.isRefreshing = false
        swipe_refresh.visibility = View.GONE
        loading_bar.visibility = View.VISIBLE
    }

    /**
     * Hides loading bar
     */
    override fun hideLoading() {
        loading_bar.visibility = View.GONE
        swipe_refresh.visibility = View.VISIBLE
        swipe_refresh.isRefreshing = false
    }

    /**
     * Show empty view, hide recycler view with contacts
     */
    private fun showEmptyView() {
        recycler_view_contacts.visibility = View.GONE
        empty_text_view.visibility = View.VISIBLE
    }

    /**
     * Hide empty view, show recycler view with contacts
     */
    private fun hideEmptyView() {
        empty_text_view.visibility = View.GONE
        recycler_view_contacts.visibility = View.VISIBLE
    }

    /**
     * Handles swiper refresh, notify presenter to load contacts
     */
    override fun onSwipeRefresh() {
        presenter.loadContacts(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.fragment_contact_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val layoutAnimationController = AnimationUtils.loadLayoutAnimation(activityContext, R.anim.layout_animation_fall_down)
        recycler_view_contacts.layoutAnimation = layoutAnimationController
        recycler_view_contacts.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        contactAdapter.setHasStableIds(true)
        recycler_view_contacts.adapter = contactAdapter
        contactAdapter.getItemOnClickObservable().subscribe({ t -> onContactClicked(t) })
        swipe_refresh.setOnRefreshListener { onSwipeRefresh() }
        presenter.attachView(this)
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onResume() {
        super.onResume()
        presenter.loadContacts(false)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.menu_contact_list, menu)
        searchView = menu?.findItem(R.id.action_search)?.actionView as SearchView
        searchView.queryHint = resourceManager.getString(R.string.search_contact)
        searchView.setQuery(searchView.query, false)
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                contactAdapter.searchContacts(query)
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                timer?.cancel()
                timer = Timer()
                timer?.schedule(object : TimerTask() {
                    override fun run() {
                        activity?.runOnUiThread({ contactAdapter.searchContacts(newText) })
                    }

                }, 300)
                return false
            }
        })
        val searchBar = searchView.findViewById<LinearLayout>(R.id.search_bar)
        searchBar.layoutTransition = LayoutTransition()
        super.onCreateOptionsMenu(menu, inflater)
    }

    /**
     * Shows no network connection noInternetConnectionSnackbar
     */
    override fun showNoNetworkConnectionIndicator() {
        noInternetConnectionSnackbar = Snackbar.make(activity!!.findViewById(R.id.content), R.string.no_network_connection, Snackbar.LENGTH_INDEFINITE)
        noInternetConnectionSnackbar?.setAction(resourceManager.getString(R.string.retry), { presenter.onRetryBtnClicked() })
        noInternetConnectionSnackbar?.show()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
    }
}
