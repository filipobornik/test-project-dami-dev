package cz.filipobornik.testproject.ui.editUserAccountActivity

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.databinding.DataBindingUtil
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.os.PersistableBundle
import android.provider.MediaStore
import android.support.design.widget.BottomSheetBehavior
import android.support.design.widget.BottomSheetDialog
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v4.content.FileProvider
import android.util.Patterns
import android.view.View
import android.widget.LinearLayout
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.yalantis.ucrop.UCrop
import cz.filipobornik.testproject.BuildConfig
import cz.filipobornik.testproject.R
import cz.filipobornik.testproject.base.BaseActivity
import cz.filipobornik.testproject.data.model.User
import cz.filipobornik.testproject.databinding.ActivityEditUserAccountBinding
import cz.filipobornik.testproject.utils.Utils
import kotlinx.android.synthetic.main.activity_edit_user_account.*
import java.io.File
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

/**
 * Activity that allow user to edit account details
 */
class EditUserAccountActivity : BaseActivity(), EditUserAccountActivityContract.View {

    /**
     * Constant represent request code for read from external storage request
     */
    private val MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE: Int = 1003

    /**
     * Constant represent request code for write external storage request
     */
    private val MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE: Int = 1004

    /**
     * Gallery request code
     */
    private val GALLERY_REQUEST_CODE: Int = 10

    /**
     * Request code for camera take image request
     */
    private val CAMERA_REQUEST_CODE: Int = 15

    /**
     * Taken image uri path into saved instance state
     */
    private val TAKEN_IMAGE_URI_PATH: String = "takenImageUriPath"

    @Inject
    lateinit var presenter: EditUserAccountActivityContract.Presenter

    /**
     * Custom user image file selected by user from gallery
     */
    private var selectedUserImageFile: File? = null

    /**
     * Currently edited user
     */
    private lateinit var user: User


    /**
     * Bottom sheet dialog that let user choose if he want to take new picture or use existing one from gallery
     */
    private lateinit var bottomSheetDialog: BottomSheetDialog

    /**
     * Uri for image taken by user
     */
    private lateinit var takenImageUri: Uri


    /**
     * Show Bottom sheet dialog and let user choose if he want to take new picture or use existing one from gallery
     */
    override fun showBottomSheetDialogCameraOrGallery() {
        bottomSheetDialog.show()
    }

    /**
     * Handle take picture button click, check permission - if not granted request them, start camera activity
     */
    override fun onTakePictureBtnClicked() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (applicationContext.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermissionWriteExternalStorage()
            } else {
                startCameraActivity()
            }
        } else {
            startCameraActivity()
        }
    }

    /**
     * Start camera activity that enables user to tak picture
     */
    override fun startCameraActivity() {
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        val file = getOutputMediaFile()
        if (file == null) {
            showError(R.string.some_error_occured)
            return
        }
        takenImageUri = if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT){
            Uri.fromFile(file)
        } else {
            FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID + ".provider", file)
        }

        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, takenImageUri)
        startActivityForResult(takePictureIntent, CAMERA_REQUEST_CODE)
    }

    /**
     * Get media output file where images taken by user will be saved
     */
    private fun getOutputMediaFile(): File? {
        val mediaStorageDir = File(getExternalFilesDir(Environment.DIRECTORY_PICTURES),
                resourceManager.getString(R.string.app_name))

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null
            }
        }

        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(Date())
        return File.createTempFile("IMG_$timeStamp", ".jpg", mediaStorageDir)
    }

    /**
     * Handle on image add button click, check if app has permissions to read from external storage
     */
    private fun onImageAddBtnClicked() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (applicationContext.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermissionReadFromExternalStorage()
            } else {
                startPhotoPickerActivity()
            }
        } else {
            startPhotoPickerActivity()
        }
    }

    /**
     * Request permission for write into external storage
     */
    private fun requestPermissionWriteExternalStorage() {
        ActivityCompat.requestPermissions(this,
                arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE)
    }

    /**
     * Request permission for reading from external storage
     */
    private fun requestPermissionReadFromExternalStorage() {
        ActivityCompat.requestPermissions(this,
                arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE)
    }


    /**
     * Handles result of request permission dialog, on error show error message
     */
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE -> {
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    onImageAddBtnClicked()
                } else {
                    showError(R.string.need_permission_for_load_image)
                }
            }
            MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE -> {
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    onTakePictureBtnClicked()
                } else {
                    showError(R.string.need_permission_for_take_image)
                }
            }
        }
    }

    /**
     * Starts photo picker activity that enable the user choose his image from gallery
     */
    override fun startPhotoPickerActivity() {

        val photoPickerIntent = Intent(Intent.ACTION_PICK)
        photoPickerIntent.type = "image/*"
        startActivityForResult(photoPickerIntent, GALLERY_REQUEST_CODE)
    }

    /**
     * Start UCrop activity that enable user to crop the image, after the compression into png is made
     */
    override fun startUCropActivity(imageUri: Uri) {
        val options = UCrop.Options()
        options.setToolbarColor(ContextCompat.getColor(this, R.color.colorPrimary))
        options.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark))
        options.setActiveWidgetColor(ContextCompat.getColor(this, R.color.colorAccent))
        options.setCompressionFormat(Bitmap.CompressFormat.PNG)

        UCrop.of(imageUri, Uri.fromFile(getOutputMediaFile()))
                .withAspectRatio(4f, 3f)
                .withOptions(options)
                .start(this)
    }

    /**
     * Handles photo picker activity response
     */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        var imageUri: Uri?

        when (requestCode) {
            GALLERY_REQUEST_CODE -> {
                if (resultCode == Activity.RESULT_OK) {
                    imageUri = data?.data
                    if (imageUri != null) {
                        startUCropActivity(imageUri)
                    } else {
                        showError(R.string.no_image_chosen)
                    }
                }
            }
            CAMERA_REQUEST_CODE -> {
                if (resultCode == Activity.RESULT_OK) {
                    imageUri = takenImageUri
                    requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT
                    startUCropActivity(imageUri)
                } else {
                    showError(R.string.no_image_chosen)
                }
            }
            UCrop.REQUEST_CROP -> {
                if (data != null && resultCode == Activity.RESULT_OK) {
                    imageUri = UCrop.getOutput(data)
                    val imageStream = contentResolver.openInputStream(imageUri)
                    val selectedUserImage = BitmapFactory.decodeStream(imageStream)
                    val options = RequestOptions()
                            .error(R.drawable.user_img_placeholder)
                            .signature(user.imageVersionSignature)

                    Glide.with(this)
                            .load(selectedUserImage)
                            .apply(options)
                            .into(img_user)
                    selectedUserImageFile = File(getImagePathFromUri(imageUri))
                } else {
                    showError(R.string.no_image_chosen)
                }
            }
        }
    }

    /**
     * Handles save user fab click and validate inputs
     */
    override fun onSaveUserFabClicked() {
        if (edit_txt_email.text.toString().isEmpty()) {
            showError(R.string.email_and_phone_are_required)
            return
        }
        if (!Utils.isPhoneNumberValid(edit_txt_phone.text.toString()) && edit_txt_phone.text.toString().isNotEmpty()) {
            showError(R.string.phone_number_not_valid)
            return
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(edit_txt_email.text.toString()).matches()) {
            showError(R.string.email_not_valid)
            return
        }
        user.name = edit_txt_name.text.toString()
        user.lastname = edit_txt_lastname.text.toString()
        user.email = edit_txt_email.text.toString()
        user.phone = edit_txt_phone.text.toString()
        user.description = edit_txt_description.text.toString()
        user.imageVersionSignature.increaseCurrentVersion()

        presenter.saveUser(user, selectedUserImageFile)
    }

    /**
     * Get image path from Uri
     */
    private fun getImagePathFromUri(imageUri: Uri?): String {
        val imagePath: String
        if (imageUri == null) {
            return ""
        }

        val cursor = contentResolver.query(imageUri, null, null, null, null)
        if (cursor == null) {
            imagePath = imageUri.path
        } else {
            cursor.moveToFirst()
            val idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
            imagePath = cursor.getString(idx)
            cursor.close()
        }
        return imagePath
    }

    /**
     * Return back to main activity
     */
    override fun goBackToMainActivity() {
        finish()
    }

    /**
     * Show loading bar
     */
    override fun showLoading() {
        container.visibility = View.GONE
        loading_bar.visibility = View.VISIBLE
    }

    /**
     * Hide loading bar
     */
    override fun hideLoading() {
        loading_bar.visibility = View.GONE
        container.visibility = View.VISIBLE
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter.attachView(this)
        setContentView(R.layout.activity_edit_user_account)
        user = userSessionManager.getLoggedUser()

        val binding = DataBindingUtil.setContentView<ActivityEditUserAccountBinding>(this, R.layout.activity_edit_user_account)
        binding.user = user

        supportActionBar?.title = resourceManager.getString(R.string.edit_user_account)

        val options = RequestOptions()
                .error(R.drawable.user_img_placeholder)
                .signature(user.imageVersionSignature)
                .placeholder(R.drawable.user_img_placeholder)

        Glide.with(this)
                .load(user.photo)
                .transition(DrawableTransitionOptions().crossFade())
                .apply(options)
                .into(img_user)

        bottomSheetDialog = BottomSheetDialog(this)
        val sheetView: View = layoutInflater.inflate(R.layout.bottom_sheet_dialog_camera_or_gallery, null)
        bottomSheetDialog.setContentView(sheetView)
        val bottomSheetBehavior = BottomSheetBehavior.from(sheetView.parent as View)
        bottomSheetDialog.setOnShowListener { bottomSheetBehavior.peekHeight = sheetView.height }
        sheetView.findViewById<LinearLayout>(R.id.item_camera).setOnClickListener {
            onTakePictureBtnClicked()
            bottomSheetDialog.dismiss()
        }
        sheetView.findViewById<LinearLayout>(R.id.item_gallery).setOnClickListener {
            onImageAddBtnClicked()
            bottomSheetDialog.dismiss()
        }

        fab_save.setOnClickListener({ onSaveUserFabClicked() })
        btn_change_user_image.setOnClickListener({ showBottomSheetDialogCameraOrGallery() })
    }

    override fun onSaveInstanceState(outState: Bundle?, outPersistentState: PersistableBundle?) {
        super.onSaveInstanceState(outState, outPersistentState)
        outState?.putString(TAKEN_IMAGE_URI_PATH, takenImageUri.toString())
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
    }
}
