package cz.filipobornik.testproject.ui.editUserAccountActivity

import android.support.v7.app.AppCompatActivity
import cz.filipobornik.testproject.base.BaseActivityModule
import cz.filipobornik.testproject.di.PerActivity
import dagger.Binds
import dagger.Module

@Module(includes = [BaseActivityModule::class])
abstract class EditUserAccountActivityModule {

    @Binds
    @PerActivity
    abstract fun provideActivity(userAccountActivity: EditUserAccountActivity): AppCompatActivity

    @Binds
    @PerActivity
    abstract fun providePresenter(presenterUserAccount: EditUserAccountActivityPresenter): EditUserAccountActivityContract.Presenter
}