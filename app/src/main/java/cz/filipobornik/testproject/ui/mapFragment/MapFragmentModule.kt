package cz.filipobornik.testproject.ui.mapFragment

import android.support.v4.app.Fragment
import cz.filipobornik.testproject.base.BaseFragmentModule
import cz.filipobornik.testproject.di.PerFragment
import dagger.Binds
import dagger.Module

@Module(includes = [BaseFragmentModule::class])
abstract class MapFragmentModule {

    @Binds
    @PerFragment
    abstract fun provideFragment(fragment: MapFragment): Fragment

    @Binds
    @PerFragment
    abstract fun providePresenter(presenter: MapFragmentPresenter): MapFragmentContract.Presenter

}