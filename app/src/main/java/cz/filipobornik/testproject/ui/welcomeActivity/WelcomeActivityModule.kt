package cz.filipobornik.testproject.ui.welcomeActivity

import android.support.v7.app.AppCompatActivity
import cz.filipobornik.testproject.base.BaseActivityModule
import cz.filipobornik.testproject.di.PerActivity
import cz.filipobornik.testproject.di.PerFragment
import cz.filipobornik.testproject.ui.aboutAppFragment.AboutAppFragment
import cz.filipobornik.testproject.ui.aboutAppFragment.AboutAppFragmentModule
import cz.filipobornik.testproject.ui.singInFragment.SignInFragment
import cz.filipobornik.testproject.ui.singInFragment.SignInFragmentModule
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module(includes = [BaseActivityModule::class])
abstract class WelcomeActivityModule {

    @Binds
    @PerActivity
    abstract fun provideActivity(activity: WelcomeActivity): AppCompatActivity

    @Binds
    @PerActivity
    abstract fun providePresenter(presenter: WelcomeActivityPresenter): WelcomeActivityContract.Presenter

    @PerFragment
    @ContributesAndroidInjector(modules = [AboutAppFragmentModule::class])
    abstract fun aboutAppFragment(): AboutAppFragment

    @PerFragment
    @ContributesAndroidInjector(modules = [SignInFragmentModule::class])
    abstract fun signInFragment(): SignInFragment

}