package cz.filipobornik.testproject.ui.contactDetailActivity

import cz.filipobornik.testproject.base.BasePresenter
import cz.filipobornik.testproject.di.PerActivity
import javax.inject.Inject

@PerActivity
class ContactDetailActivityPresenter @Inject constructor() : BasePresenter<ContactDetailActivityContract.View>(), ContactDetailActivityContract.Presenter {


}