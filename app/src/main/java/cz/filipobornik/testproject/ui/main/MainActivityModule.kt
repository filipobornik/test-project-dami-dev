package cz.filipobornik.testproject.ui.main

import android.support.v7.app.AppCompatActivity
import cz.filipobornik.testproject.base.BaseActivityModule
import cz.filipobornik.testproject.di.PerActivity
import cz.filipobornik.testproject.di.PerFragment
import cz.filipobornik.testproject.ui.aboutAppFragment.AboutAppFragment
import cz.filipobornik.testproject.ui.aboutAppFragment.AboutAppFragmentModule
import cz.filipobornik.testproject.ui.contactListFragment.ContactListFragment
import cz.filipobornik.testproject.ui.contactListFragment.ContactListFragmentModule
import cz.filipobornik.testproject.ui.mapFragment.MapFragment
import cz.filipobornik.testproject.ui.mapFragment.MapFragmentModule
import cz.filipobornik.testproject.ui.videoFragment.VideoFragment
import cz.filipobornik.testproject.ui.videoFragment.VideoFragmentModule
import cz.filipobornik.testproject.ui.webviewFragment.WebviewFragment
import cz.filipobornik.testproject.ui.webviewFragment.WebviewFragmentModule
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * DI Module for Main Activity
 */
@Module(includes = [BaseActivityModule::class])
abstract class MainActivityModule {

    @Binds
    @PerActivity
    abstract fun provideActivity(activity: MainActivity): AppCompatActivity

    @Binds
    @PerActivity
    abstract fun providePresenter(presenter: MainActivityPresenter): MainActivityContract.Presenter

    @PerFragment
    @ContributesAndroidInjector(modules = [AboutAppFragmentModule::class])
    abstract fun aboutAppFragment(): AboutAppFragment

    @PerFragment
    @ContributesAndroidInjector(modules = [ContactListFragmentModule::class])
    abstract fun contactListFragment(): ContactListFragment

    @PerFragment
    @ContributesAndroidInjector(modules = [WebviewFragmentModule::class])
    abstract fun webviewFragment(): WebviewFragment

    @PerFragment
    @ContributesAndroidInjector(modules = [MapFragmentModule::class])
    abstract fun mapFragment(): MapFragment

    @PerFragment
    @ContributesAndroidInjector(modules = [VideoFragmentModule::class])
    abstract fun videoFragment(): VideoFragment

}