package cz.filipobornik.testproject.ui.videoFragment

import cz.filipobornik.testproject.base.BaseMVPPresenter
import cz.filipobornik.testproject.base.BaseMVPView

abstract class VideoFragmentContract {

    interface View : BaseMVPView {

    }

    interface Presenter : BaseMVPPresenter<View> {

    }

}