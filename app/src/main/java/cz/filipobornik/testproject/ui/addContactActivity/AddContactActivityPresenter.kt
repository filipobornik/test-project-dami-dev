package cz.filipobornik.testproject.ui.addContactActivity

import android.content.res.Resources
import cz.filipobornik.testproject.R
import cz.filipobornik.testproject.base.BasePresenter
import cz.filipobornik.testproject.data.DataManager
import cz.filipobornik.testproject.data.model.Contact
import cz.filipobornik.testproject.data.remote.NetworkMonitor
import cz.filipobornik.testproject.di.PerActivity
import cz.filipobornik.testproject.utils.ErrorUtils
import java.io.File
import javax.inject.Inject

@PerActivity
class AddContactActivityPresenter @Inject constructor(
        private val dataManager: DataManager,
        private val resourceManager: Resources,
        private val errorUtils: ErrorUtils,
        private val networkMonitor: NetworkMonitor)
    : BasePresenter<AddContactActivityContract.View>(), AddContactActivityContract.Presenter {

    /**
     * Save user, on success notify view, on error notify view to show specific error message
     */
    override fun saveContact(contact: Contact, selectedContactImageFile: File?) {
        view?.showLoading()

        if (!networkMonitor.isConnected) {
            view?.hideLoading()
            view?.showNoNetworkConnectionIndicator()
        } else {
            dataManager.addContact(contact, selectedContactImageFile)
                    .subscribe({ response ->
                        run {
                            view?.showMessage(resourceManager.getString(R.string.contact_added))
                            view?.goBackToAccountDetailActivity()
                        }
                    }, { error ->
                        run {
                            view?.hideLoading()
                            view?.showError(errorUtils.returnApiErrorMessage(error))
                        }
                    })
        }
    }

    /**
     * Handle retry button click, notify view to try save contact again
     */
    override fun onRetryBtnClicked() {
        view?.onSaveContactFabClicked()
    }
}