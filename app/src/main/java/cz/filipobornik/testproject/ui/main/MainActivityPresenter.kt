package cz.filipobornik.testproject.ui.main

import cz.filipobornik.testproject.base.BasePresenter
import cz.filipobornik.testproject.data.DataManager
import cz.filipobornik.testproject.data.remote.NetworkMonitor
import cz.filipobornik.testproject.di.PerActivity
import cz.filipobornik.testproject.utils.ErrorUtils
import cz.filipobornik.testproject.utils.UserSessionManager
import javax.inject.Inject

@PerActivity
class MainActivityPresenter @Inject constructor(
        private val userSessionManager: UserSessionManager,
        private val networkMonitor: NetworkMonitor,
        private val dataManager: DataManager,
        private val errorUtils: ErrorUtils)
    : BasePresenter<MainActivityContract.View>(), MainActivityContract.Presenter {

    /**
     * Chech if user is logged in, if not notify view to show WelcomeActivity and tries to update contacts in db
     */
    override fun checkLogin() {
        if (!userSessionManager.isUserLoggedIn()) {
            view?.startTutorialActivity()
        } else {
            // Download latest version of contacts on app launch, if session is expired let user to login again
            if (networkMonitor.isConnected) {
                dataManager.syncContacts().subscribe(
                        {},
                        { t ->
                            if (!errorUtils.isSessionTokenActive(t)) {
                                userSessionManager.handleLogOut()
                                view?.startWelcomeActivity()
                            }
                        })
            }
        }
    }

    /**
     * Handle nav log out item click, notify user session manager to handle logout
     */
    override fun navLogOutItemClicked() {
        userSessionManager.handleLogOut()
        checkLogin()
    }

}