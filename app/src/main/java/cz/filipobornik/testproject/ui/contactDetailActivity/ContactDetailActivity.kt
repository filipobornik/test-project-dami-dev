package cz.filipobornik.testproject.ui.contactDetailActivity

import android.Manifest
import android.annotation.SuppressLint
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.databinding.DataBindingUtil
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v4.view.MenuItemCompat
import android.view.Menu
import android.view.MenuItem
import android.widget.ShareActionProvider
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import cz.filipobornik.testproject.BuildConfig
import cz.filipobornik.testproject.R
import cz.filipobornik.testproject.base.BaseActivity
import cz.filipobornik.testproject.data.model.Contact
import cz.filipobornik.testproject.databinding.ActivityContactDetailBinding
import cz.filipobornik.testproject.utils.Constants
import cz.filipobornik.testproject.utils.Utils
import kotlinx.android.synthetic.main.activity_contact_detail.*
import javax.inject.Inject

/**
 * Activity that displays selected contact's account details
 */
class ContactDetailActivity : BaseActivity(), ContactDetailActivityContract.View {

    @Inject
    lateinit var presenter: ContactDetailActivityContract.Presenter

    private lateinit var shareActionProvider: ShareActionProvider

    companion object {
        val REQUEST_CODE_ASK_PERMISSIONS: Int = 1234
    }

    /**
     * Contact that details are displayed
     */
    private lateinit var contact: Contact

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contact_detail)
        presenter.attachView(this)

        val extras = intent.extras
        contact = if (extras != null) {
            extras.getParcelable(Constants.EXTRA_CONTACT)
        } else {
            showError(R.string.unable_to_load_contact_details)
            Contact(-1, null, null, null, null, null, null)
        }

        initViews()
    }

    /**
     * Creates share intent and let user to choose app
     */
    override fun shareContact() {
        val sendIntent = Intent()
        sendIntent.action = Intent.ACTION_SEND
        sendIntent.putExtra(Intent.EXTRA_TEXT, prepareDataToShare())
        sendIntent.type = "text/plain"
        startActivity(sendIntent)
    }

    /**
     * Format data to share into other apps
     */
    private fun prepareDataToShare(): String {
        return resourceManager.getString(R.string.contact_name) + ": \n${contact.name} ${contact.lastname}\n" +
                resourceManager.getString(R.string.phone_number) + ": ${Utils.formatPhoneNumber(contact.phone)}\n" +
                resourceManager.getString(R.string.email) + ": ${contact.email}"
    }

    /**
     * Let user choose app to send email to the selected contact
     */
    override fun sendEmail() {
        if(contact.email.isNullOrEmpty()) {
            showError(R.string.contact_without_email)
        } else {
            val intent = Intent(Intent.ACTION_SENDTO)
            intent.type = "message/rfc822"
            intent.data = Uri.parse("mailto:${contact.email}")
            intent.putExtra(Intent.EXTRA_SUBJECT, resourceManager.getString(R.string.email_from_dami_dev_app))
            intent.putExtra(Intent.EXTRA_TEXT, "\n\n${resourceManager.getString(R.string.greeting)}, " +
                    "${userSessionManager.getLoggedUser().name} ${userSessionManager.getLoggedUser().lastname}")
            startActivity(Intent.createChooser(intent, resourceManager.getString(R.string.send_email)))
        }
    }

    /**
     * Initialize views and it's values
     */
    override fun initViews() {

        val binding = DataBindingUtil.setContentView<ActivityContactDetailBinding>(this, R.layout.activity_contact_detail)
        binding.contact = contact

        val emptyTxtViewIndicator = resourceManager.getString(R.string.empty_txt_view_indicator)
        val fullName = "${contact.name} ${contact.lastname}"
        if (fullName.isEmpty()) {
            collapsing_toolbar_layout?.title = emptyTxtViewIndicator
        } else {
            collapsing_toolbar_layout?.title = fullName
        }
        val options = RequestOptions()
                .error(R.drawable.user_img_placeholder)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        Glide.with(this)
                .load(contact.photo ?: "")
                .apply(options)
                .into(img_user)

        txt_email.setOnClickListener { copyToClipBoard(resourceManager.getString(R.string.email), txt_email.text.toString()) }
        layoutPhoneNumber.setOnClickListener { callNumber(txt_phone.text.toString()) }
    }

    /**
     * Copy text to clipboard
     */
    private fun copyToClipBoard(label: String, text: String) {
        val clipboard = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        val clipData = ClipData.newPlainText(label, text)
        clipboard.primaryClip = clipData
        showMessage(R.string.copied_to_clipboard)
    }

    private fun callNumber(number: String) {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.CALL_PHONE), REQUEST_CODE_ASK_PERMISSIONS)
        } else {
            val callIntent = Intent(Intent.ACTION_CALL)
            callIntent.data = Uri.parse("tel:" + number)
            startActivity(callIntent)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if(requestCode == REQUEST_CODE_ASK_PERMISSIONS) {
            layoutPhoneNumber.callOnClick()
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_contact_detail, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
            R.id.action_share -> {
                shareContact()
            }
            R.id.action_email -> {
                sendEmail()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
    }
}
