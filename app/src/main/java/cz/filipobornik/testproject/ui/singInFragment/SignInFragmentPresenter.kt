package cz.filipobornik.testproject.ui.singInFragment

import android.content.res.Resources
import android.os.Bundle
import com.facebook.AccessToken
import com.facebook.GraphRequest
import com.facebook.login.LoginResult
import com.google.gson.Gson
import cz.filipobornik.testproject.R
import cz.filipobornik.testproject.base.BasePresenter
import cz.filipobornik.testproject.data.DataManager
import cz.filipobornik.testproject.data.model.Response
import cz.filipobornik.testproject.data.model.User
import cz.filipobornik.testproject.data.remote.NetworkMonitor
import cz.filipobornik.testproject.di.PerFragment
import cz.filipobornik.testproject.utils.ErrorUtils
import cz.filipobornik.testproject.utils.ImageVersionSignature
import cz.filipobornik.testproject.utils.UserSessionManager
import retrofit2.HttpException
import javax.inject.Inject


@PerFragment
class SignInFragmentPresenter @Inject constructor(
        private val dataManager: DataManager,
        private val resourceManager: Resources,
        private val errorUtils: ErrorUtils,
        private val networkMonitor: NetworkMonitor,
        private val userSessionManager: UserSessionManager)
    : BasePresenter<SignInFragmentContract.View>(), SignInFragmentContract.Presenter {

    private var userAccessToken: AccessToken? = null

    /**
     * Handle button retry click
     */
    override fun onRetryBtnClicked() {
        if (!networkMonitor.isConnected) {
            view?.showNoNetworkConnectionIndicator()
        } else {
            view?.onBtnSignInClicked()
        }
    }

    /**
     * Handle on btn facebook sign in click
     */
    override fun onBtnFacebookSignInClick(loginResult: LoginResult) {
        view?.showLoading()
        userAccessToken = loginResult.accessToken
        val request = GraphRequest.newMeRequest(userAccessToken) { jsonObject, response ->
            val fId = if (jsonObject.has("id")) jsonObject.getString("id") else ""
            val email = if (jsonObject.has("email")) jsonObject.getString("email") else ""

            signInUserByFacebook(email, fId)
        }
        val parameters = Bundle()
        parameters.putString("fields", "id, first_name, last_name, email")
        request.parameters = parameters
        request.executeAsync()
    }

    /**
     * Sign in user by facebook, if account does't exist create new one
     */
    private fun signInUserByFacebook(email: String, fId: String) {
        compositeDisposable.add(dataManager.fbSignIn(email, fId)
                .subscribe({ user ->
                    run {
                        if (user != null) {
                            updateUserProfileWithFacebookData()
                            successfulLogin(user)
                        }
                    }
                }, { error ->
                    run {
                        view?.hideLoading()
                        if (error is HttpException) {
                            val errorBodyJSON = error.response().errorBody()?.string()
                            if (Gson().fromJson(errorBodyJSON, Response::class.java).responseCode == 7) {
                                registerUserByFacebook(email, fId)
                            } else {
                                view?.showError(errorUtils.returnApiErrorMessage(error))
                            }

                        }
                    }
                }))
    }

    /**
     * Register user by facebook account
     */
    private fun registerUserByFacebook(email: String, fId: String) {
        compositeDisposable.add(dataManager.fbRegister(email, fId)
                .subscribe({ user ->
                    run {
                        if (user != null) {
                            userSessionManager.handleLogin(user)
                            updateUserProfileWithFacebookData()
                            successfulLogin(user)
                        }
                    }
                }, { error ->
                    run {
                        view?.hideLoading()
                        if (error is HttpException) {
                            view?.showError(errorUtils.returnApiErrorMessage(error))
                        }
                    }
                }))
    }

    /**
     * Update user profile with data from user's facebook account
     */
    private fun updateUserProfileWithFacebookData() {
        val request = GraphRequest.newMeRequest(userAccessToken) { jsonObject, response ->
            val fId = if (jsonObject.has("id")) jsonObject.getString("id") else ""
            val firstName = if (jsonObject.has("first_name")) jsonObject.getString("first_name") else ""
            val lastName = if (jsonObject.has("last_name")) jsonObject.getString("last_name") else ""
            val email = if (jsonObject.has("email")) jsonObject.getString("email") else ""

            val user = User(0, firstName, lastName, email)

            compositeDisposable.add(dataManager.updateAccount(user, null)
                    .subscribe({ _ ->
                        run {
                            userSessionManager.updateUser(user)
                        }
                    }, { error ->
                        run {
                            view?.hideLoading()
                            if (error is HttpException) {
                                view?.showError(errorUtils.returnApiErrorMessage(error))
                            }
                        }
                    }))
        }
        val parameters = Bundle()
        parameters.putString("fields", "id, first_name, last_name, email")
        request.parameters = parameters
        request.executeAsync()
    }

    /**
     *  Sign user in, on success notifies view to start MainActivity, on error notify view to show error message
     */
    override fun onBtnSignInClick(email: String, password: String) {
        view?.showLoading()
        if (!networkMonitor.isConnected) {
            view?.showNoNetworkConnectionIndicator()
        }
        compositeDisposable.add(dataManager.signIn(email, password)
                .subscribe({ user ->
                    run {
                        if (user != null) {
                            successfulLogin(user)
                        }
                    }
                }, { error ->
                    run {
                        view?.hideLoading()
                        if (error is HttpException) {
                            view?.showError(errorUtils.returnApiErrorMessage(error))
                        }
                    }
                }))
    }

    /**
     * Handle successful login
     */
    private fun successfulLogin(user: User) {
        view?.showMessage(resourceManager.getString(R.string.login_successful))
        user.imageVersionSignature = ImageVersionSignature()
        userSessionManager.handleLogin(user)

        // Download latest version of contacts on successful login
        dataManager.syncContacts().subscribe(
                {
                    view?.startMainActivity()
                    view?.hideLoading()
                },
                {
                    view?.startMainActivity()
                    view?.hideLoading()
                })
    }

    /**
     * Notifies view to start RegistrationActivity
     */
    override fun onBtnRegisterClicked() {
        view?.startRegistrationActivity()
    }
}