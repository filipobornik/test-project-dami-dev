package cz.filipobornik.testproject.ui.registrationActivity

import android.content.res.Resources
import android.util.Log
import cz.filipobornik.testproject.R
import cz.filipobornik.testproject.base.BasePresenter
import cz.filipobornik.testproject.data.DataManager
import cz.filipobornik.testproject.data.remote.NetworkMonitor
import cz.filipobornik.testproject.di.PerActivity
import cz.filipobornik.testproject.utils.ErrorUtils
import cz.filipobornik.testproject.utils.UserSessionManager
import javax.inject.Inject

@PerActivity
class RegistrationActivityPresenter @Inject constructor(
        private val dataManager: DataManager,
        private val networkMonitor: NetworkMonitor,
        private val resourceManager: Resources,
        private val userSessionManager: UserSessionManager,
        private val errorUtils: ErrorUtils)
    : BasePresenter<RegistrationActivityContract.View>(), RegistrationActivityContract.Presenter {

    /**
     * Handles registration when button register is clicked, on success notify view to start MainActivity,
     * on error notify view to show error message
     */
    override fun onBtnRegisterClicked(email: String, password: String) {
        if (!networkMonitor.isConnected) {
            view?.showNoNetworkConnectionIndicator()
        }
        view?.showLoading()
        dataManager.register(email, password)
                .subscribe({ user ->
                    run {
                        Log.d("test", "code come here")
                        if (user != null) {
                            view?.showMessage(resourceManager.getString(R.string.registration_successful))
                            userSessionManager.handleLogin(user)
                            view?.startMainActivity()
                            view?.hideLoading()
                        }
                    }
                }, { error ->
                    run {
                        view?.hideLoading()
                        view?.showError(errorUtils.returnApiErrorMessage(error))
                    }
                })
    }

    /**
     * Try to register again when btn retry in snackbar is clicked
     */
    override fun onRetryBtnClicked() {
        view?.onBtnRegisterClicked()
    }
}