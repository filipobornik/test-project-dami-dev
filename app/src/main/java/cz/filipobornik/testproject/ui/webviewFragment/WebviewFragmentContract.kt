package cz.filipobornik.testproject.ui.webviewFragment

import cz.filipobornik.testproject.base.BaseMVPPresenter
import cz.filipobornik.testproject.base.BaseMVPView

abstract class WebviewFragmentContract {

    interface View : BaseMVPView {

        fun showLoading()

        fun hideLoading()

    }

    interface Presenter : BaseMVPPresenter<View> {

    }

}