package cz.filipobornik.testproject.ui.registrationActivity

import android.support.v7.app.AppCompatActivity
import cz.filipobornik.testproject.base.BaseActivityModule
import cz.filipobornik.testproject.di.PerActivity
import dagger.Binds
import dagger.Module

@Module(includes = [BaseActivityModule::class])
abstract class RegistrationActivityModule {

    @Binds
    @PerActivity
    abstract fun provideActivity(registrationActivity: RegistrationActivity): AppCompatActivity

    @Binds
    @PerActivity
    abstract fun providePresenter(presenter: RegistrationActivityPresenter): RegistrationActivityContract.Presenter

}