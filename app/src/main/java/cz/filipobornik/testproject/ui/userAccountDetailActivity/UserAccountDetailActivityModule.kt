package cz.filipobornik.testproject.ui.userAccountDetailActivity

import android.support.v7.app.AppCompatActivity
import cz.filipobornik.testproject.base.BaseActivityModule
import cz.filipobornik.testproject.di.PerActivity
import dagger.Binds
import dagger.Module

@Module(includes = [BaseActivityModule::class])
abstract class UserAccountDetailActivityModule {

    @Binds
    @PerActivity
    abstract fun provideActivity(activity: UserAccountDetailActivity): AppCompatActivity

    @Binds
    @PerActivity
    abstract fun providePresenter(preseter: UserAccountDetailActivityPresenter): UserAccountDetailActivityContract.Presenter
}