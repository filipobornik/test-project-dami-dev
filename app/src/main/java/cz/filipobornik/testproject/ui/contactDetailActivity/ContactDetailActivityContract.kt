package cz.filipobornik.testproject.ui.contactDetailActivity

import cz.filipobornik.testproject.base.BaseMVPPresenter
import cz.filipobornik.testproject.base.BaseMVPView

class ContactDetailActivityContract {

    interface View : BaseMVPView {

        fun initViews()

        fun shareContact()

        fun sendEmail()

    }

    interface Presenter : BaseMVPPresenter<View>

}