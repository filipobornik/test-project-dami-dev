package cz.filipobornik.testproject.ui.mapFragment

import cz.filipobornik.testproject.base.BaseMVPPresenter
import cz.filipobornik.testproject.base.BaseMVPView

abstract class MapFragmentContract {

    interface View : BaseMVPView {

    }

    interface Presenter : BaseMVPPresenter<View> {

    }

}