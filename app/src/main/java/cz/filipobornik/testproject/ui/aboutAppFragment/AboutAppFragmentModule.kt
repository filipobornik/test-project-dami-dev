package cz.filipobornik.testproject.ui.aboutAppFragment

import android.support.v4.app.Fragment
import cz.filipobornik.testproject.base.BaseFragmentModule
import cz.filipobornik.testproject.di.PerFragment
import dagger.Binds
import dagger.Module

@Module(includes = [BaseFragmentModule::class])
abstract class AboutAppFragmentModule {

    @Binds
    @PerFragment
    abstract fun provideFragment(fragment: AboutAppFragment): Fragment

    @Binds
    @PerFragment
    abstract fun providePresenter(presenter: AboutAppFragmentPresenter): AboutAppFragmentContract.Presenter

}