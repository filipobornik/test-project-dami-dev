package cz.filipobornik.testproject.ui.contactListFragment

import cz.filipobornik.testproject.base.BasePresenter
import cz.filipobornik.testproject.data.DataManager
import cz.filipobornik.testproject.data.remote.NetworkMonitor
import cz.filipobornik.testproject.di.PerFragment
import cz.filipobornik.testproject.utils.ErrorUtils
import javax.inject.Inject

@PerFragment
class ContactListFragmentPresenter @Inject constructor(
        private val dataManager: DataManager,
        private val errorUtils: ErrorUtils,
        private val networkMonitor: NetworkMonitor)
    : BasePresenter<ContactListFragmentContract.View>(), ContactListFragmentContract.Presenter {

    /**
     * Load contacts, on success notifies view to show them on the screen, on error notify view to show error message
     */
    override fun loadContacts(forceUpdate: Boolean) {
        view?.showLoading()

        if (!networkMonitor.isConnected) {
            view?.showNoNetworkConnectionIndicator()
        }

        if (!forceUpdate || !networkMonitor.isConnected) {
            view?.hideLoading()
            view?.showContacts(ArrayList(dataManager.getContacts()))
        } else {
            compositeDisposable.add(dataManager.syncContacts()
                    .subscribe({ contacts ->
                        run {
                            view?.hideLoading()
                            if (contacts != null) {
                                view?.showContacts(contacts)
                            } else {
                                view?.showContacts(arrayListOf())
                            }
                        }
                    }, { error ->
                        run {
                            view?.hideLoading()
                            if (!errorUtils.isSessionTokenActive(error)) {
                                view?.onTokenExpire()
                            } else {
                                view?.showError(errorUtils.returnApiErrorMessage(error))
                            }
                        }
                    }))
        }
    }

    /**
     * Handles on retry button click, try loads contacts again
     */
    override fun onRetryBtnClicked() {
        loadContacts(forceUpdate = true)
    }

}