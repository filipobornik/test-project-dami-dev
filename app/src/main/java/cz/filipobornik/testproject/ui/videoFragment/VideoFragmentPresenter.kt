package cz.filipobornik.testproject.ui.videoFragment

import cz.filipobornik.testproject.base.BasePresenter
import cz.filipobornik.testproject.di.PerFragment

@PerFragment
class VideoFragmentPresenter : BasePresenter<VideoFragmentContract.View>(), VideoFragmentContract.Presenter {
}