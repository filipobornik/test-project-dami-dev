package cz.filipobornik.testproject.ui.splashScreenActivity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.content.Intent
import cz.filipobornik.testproject.ui.main.MainActivity

/**
 * Splash screen activity displays logo
 */
class SplashScreenActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }
}
