package cz.filipobornik.testproject.ui.welcomeActivity

import cz.filipobornik.testproject.base.BaseMVPPresenter
import cz.filipobornik.testproject.base.BaseMVPView

abstract class WelcomeActivityContract {

    interface View : BaseMVPView

    interface Presenter : BaseMVPPresenter<View>

}