package cz.filipobornik.testproject.ui.singInFragment

import com.facebook.login.LoginResult
import cz.filipobornik.testproject.base.BaseMVPPresenter
import cz.filipobornik.testproject.base.BaseMVPView

class SignInFragmentContract {

    interface View : BaseMVPView {

        fun startMainActivity()

        fun startRegistrationActivity()

        fun showLoading()

        fun hideLoading()

        fun showNoNetworkConnectionIndicator()

        fun onBtnSignInClicked()

        fun onBtnRegisterClicked()

    }

    interface Presenter : BaseMVPPresenter<View> {

        fun onBtnSignInClick(email: String, password: String)

        fun onBtnFacebookSignInClick(loginResult: LoginResult)

        fun onBtnRegisterClicked()

        fun onRetryBtnClicked()

    }
}