package cz.filipobornik.testproject.ui.aboutAppFragment

import cz.filipobornik.testproject.base.BasePresenter
import cz.filipobornik.testproject.di.PerFragment
import javax.inject.Inject

@PerFragment
class AboutAppFragmentPresenter @Inject constructor() : BasePresenter<AboutAppFragmentContract.View>(), AboutAppFragmentContract.Presenter {

}