package cz.filipobornik.testproject.ui.welcomeActivity

import cz.filipobornik.testproject.base.BasePresenter
import cz.filipobornik.testproject.di.PerActivity
import javax.inject.Inject

@PerActivity
class WelcomeActivityPresenter @Inject constructor() : BasePresenter<WelcomeActivityContract.View>(), WelcomeActivityContract.Presenter {
}