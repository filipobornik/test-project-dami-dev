package cz.filipobornik.testproject.ui.webviewFragment

import cz.filipobornik.testproject.base.BasePresenter
import javax.inject.Inject

class WebviewFragmentPresenter @Inject constructor(): BasePresenter<WebviewFragmentContract.View>(), WebviewFragmentContract.Presenter {
}