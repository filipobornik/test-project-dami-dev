package cz.filipobornik.testproject.ui.mapFragment

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.*
import cz.filipobornik.testproject.R
import cz.filipobornik.testproject.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_map.*

/**
 * MapFragment that shows Dami Development s.r.o. offices and user's current location on the map
 */
class MapFragment : BaseFragment(), MapFragmentContract.View,
        OnMapReadyCallback, GoogleMap.OnMyLocationClickListener {

    /**
     * Latitude and longitude of Dami development s.r.o. offices in Hradec Kralove, Czech Republic
     */
    private val damiDevHK = LatLng(50.2110556, 15.838805555555556)

    /**
     * Latitude and longitude of Dami development s.r.o. offices in Prague, Czech Republic
     */
    private val damiDevPrg = LatLng(50.107193, 14.453449)

    /**
     * Latitude and longitude in center of Dami development offices, Czech Republic
     */
    private val center = LatLng(50.138512, 15.120739)

    /**
     * Google map
     */
    private var map: GoogleMap? = null

    /**
     * Permission request code for location permission
     */
    private val REQUEST_CODE_LOCATION = 123

    /**
     * Handle btnHK click, move camera to specific position, zoom in and rotate by 30 degrees
     */
    private fun onBtnHKClicked() {
        val cameraPosition = CameraPosition.builder()
                .target(damiDevHK)
                .zoom(17f)
                .tilt(30f)
                .build()
        map?.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition), 2000, null)
    }

    /**
     * Handle btnPRG click, move camera to specific position, zoom in and rotate by 30 degrees
     */
    private fun onBtnPRGClicked() {
        val cameraPosition = CameraPosition.builder()
                .target(damiDevPrg)
                .zoom(17f)
                .tilt(30f)
                .build()
        map?.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition), 2000, null)
    }

    /**
     * Handle onMyLocationBtn click, move camera to specific position, zoom in and rotate by 30 degrees
     */
    override fun onMyLocationClick(location: Location) {
        val cameraPosition = CameraPosition.builder()
                .target(LatLng(location.latitude, location.longitude))
                .zoom(17f)
                .tilt(30f)
                .build()
        map?.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition), 2000, null)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_map, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btnHK.setOnClickListener { onBtnHKClicked() }
        btnPRG.setOnClickListener { onBtnPRGClicked() }

        mapView.onCreate(null)
        mapView.getMapAsync(this)
    }

    override fun onResume() {
        super.onResume()
        mapView.onResume()
    }

    override fun onStart() {
        super.onStart()
        mapView.onStart()
    }

    override fun onPause() {
        mapView.onPause()
        super.onPause()
    }

    override fun onStop() {
        mapView.onStop()
        super.onStop()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        mapView.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapView.onLowMemory()
    }

    /**
     * When map is ready add markers, current location and move camera
     */
    override fun onMapReady(googleMap: GoogleMap?) {
        this.map = googleMap
        map?.setMinZoomPreference(8f)

        map?.moveCamera(CameraUpdateFactory.newLatLng(center))
        map?.addMarker(MarkerOptions()
                .position(damiDevHK)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))
                .title(resourceManager.getString(R.string.dami_dev))
        )
        map?.addMarker(MarkerOptions()
                .position(damiDevPrg)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))
                .title(resourceManager.getString(R.string.dami_dev))
        )

        if (ContextCompat.checkSelfPermission(activityContext, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            map?.isMyLocationEnabled = true
        } else {
            Log.d("Permas", "requested")
            requestPermissions(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    REQUEST_CODE_LOCATION)
        }
    }

    @SuppressLint("MissingPermission")
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_CODE_LOCATION) {
            if (permissions.size == 1 &&
                    permissions[0] == Manifest.permission.ACCESS_FINE_LOCATION &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                map?.isMyLocationEnabled = true
            } else {
                showError(R.string.need_permission_for_location)
            }
        }
    }
}
