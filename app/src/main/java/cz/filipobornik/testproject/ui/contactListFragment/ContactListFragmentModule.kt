package cz.filipobornik.testproject.ui.contactListFragment

import android.support.v4.app.Fragment
import cz.filipobornik.testproject.base.BaseFragmentModule
import cz.filipobornik.testproject.di.PerFragment
import dagger.Binds
import dagger.Module

@Module(includes = [BaseFragmentModule::class])
abstract class ContactListFragmentModule {

    @Binds
    @PerFragment
    abstract fun provideFragment(fragment: ContactListFragment): Fragment

    @Binds
    @PerFragment
    abstract fun providePresenter(presenter: ContactListFragmentPresenter): ContactListFragmentContract.Presenter

}