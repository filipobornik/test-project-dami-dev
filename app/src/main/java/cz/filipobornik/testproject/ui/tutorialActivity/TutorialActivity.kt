package cz.filipobornik.testproject.ui.tutorialActivity

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.ValueAnimator
import android.content.Intent
import android.graphics.Point
import android.graphics.drawable.Animatable
import android.os.Bundle
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.view.animation.AccelerateInterpolator
import android.view.animation.AnimationUtils
import cz.filipobornik.testproject.R
import cz.filipobornik.testproject.ui.welcomeActivity.WelcomeActivity
import kotlinx.android.synthetic.main.activity_tutorial.*
import kotlinx.android.synthetic.main.item_tutorial_about.*
import kotlinx.android.synthetic.main.item_tutorial_coffe.*
import kotlinx.android.synthetic.main.item_tutorial_help.*
import kotlinx.android.synthetic.main.item_tutorial_made_with_love.*


class TutorialActivity : AppCompatActivity(), ViewPager.OnPageChangeListener {

    private var viewpagerFirstShow = true

    private fun startWelcomeActivity() {
        val intent = Intent(this, WelcomeActivity::class.java)
        startActivity(intent)
        finish()
    }

    override fun onPageScrollStateChanged(state: Int) {
    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
        if(viewpagerFirstShow && positionOffset == 0f) {
            onPageSelected(0)
            viewpagerFirstShow = false
        }
    }

    override fun onPageSelected(position: Int) {
        if (viewpager.currentItem == viewpager.adapter?.count?.minus(1)) {
            btnNext.text = resources.getText(R.string.here_we_go)
        } else {
            btnNext.text = resources.getText(R.string.next)
        }

        when (position) {
            0 -> {
                Log.d("load", "anim")
                val loadAnimation = AnimationUtils.loadAnimation(applicationContext, android.R.anim.fade_in)
                loadAnimation.duration = 1000
                txtWelcome.startAnimation(loadAnimation)
                val animation = imgAndroid.drawable
                if (animation is Animatable) {
                    animation.start()
                }
            }
            1 -> {
                val loadAnimation = AnimationUtils.loadAnimation(applicationContext, android.R.anim.fade_in)
                loadAnimation.duration = 1000
                txtWith.startAnimation(loadAnimation)
                txtLove
                        .startAnimation(loadAnimation)

                val animation = imgLove.drawable
                if (animation is Animatable) {
                    animation.start()
                }
            }
            2 -> {
                val loadAnimation = AnimationUtils.loadAnimation(applicationContext, android.R.anim.fade_in)
                loadAnimation.duration = 1000
                txtMade.startAnimation(loadAnimation)
                txtCoffe.startAnimation(loadAnimation)

                val animation = imgLoveCoffe.drawable
                if (animation is Animatable) {
                    animation.start()
                }
            }
            3 -> {
                val animation = imgDuck.drawable
                if (animation is Animatable) {
                    animation.start()
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tutorial)

        viewpager.addOnPageChangeListener(this)
        viewpager.adapter = TutorialViewpagerAdapter(resources, this)
        indicator.setViewPager(viewpager)

        btnNext.setOnClickListener { _ ->
            run {
                if (viewpager.currentItem != viewpager.adapter?.count?.minus(1)) {
                    viewpager.currentItem = viewpager.currentItem + 1
                } else {
                    // throw away last tutorial item and start WelcomeActivity
                    btnNext.visibility = View.GONE
                    val size = Point()
                    windowManager.defaultDisplay.getSize(size)
                    val valueAnimator = ValueAnimator.ofFloat(0f, -size.y.toFloat())
                    valueAnimator.addUpdateListener {
                        val value = it.animatedValue as Float
                        cardView.translationY = value
                    }
                    valueAnimator.addListener(object : AnimatorListenerAdapter() {
                        override fun onAnimationEnd(animation: Animator) {
                            startWelcomeActivity()
                        }
                    })

                    valueAnimator.interpolator = AccelerateInterpolator(1.5f)
                    valueAnimator.duration = 500
                    valueAnimator.start()
                }
            }
        }

    }

    override fun onResume() {
        super.onResume()
    }


}
