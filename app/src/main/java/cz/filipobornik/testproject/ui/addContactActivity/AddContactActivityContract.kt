package cz.filipobornik.testproject.ui.addContactActivity

import android.net.Uri
import cz.filipobornik.testproject.base.BaseMVPPresenter
import cz.filipobornik.testproject.base.BaseMVPView
import cz.filipobornik.testproject.data.model.Contact
import java.io.File

abstract class AddContactActivityContract {

    interface View : BaseMVPView {

        fun startPhotoPickerActivity()

        fun startCameraActivity()

        fun startUCropActivity(imageUri: Uri)

        fun showBottomSheetDialogCameraOrGallery()

        fun initViews()

        fun showLoading()

        fun hideLoading()

        fun hideKeyboard()

        fun onTakePictureBtnClicked()

        fun onSaveContactFabClicked()

        fun showNoNetworkConnectionIndicator()

        fun goBackToAccountDetailActivity()

    }

    interface Presenter : BaseMVPPresenter<View> {

        fun saveContact(contact: Contact, selectedContactImageFile: File?)

        fun onRetryBtnClicked()

    }

}