package cz.filipobornik.testproject.ui.webviewFragment

import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.webkit.WebViewClient
import cz.filipobornik.testproject.R
import cz.filipobornik.testproject.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_webview.*
import javax.inject.Inject

/**
 * Simple webview fragment that shows DamiDevelopment website
 */
class WebviewFragment : BaseFragment(), WebviewFragmentContract.View {

    @Inject
    lateinit var presenter: WebviewFragmentContract.Presenter

    override fun showLoading() {
        webview.visibility = View.GONE
        loadingBar.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        loadingBar.visibility = View.GONE
        webview.visibility = View.VISIBLE
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_webview, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.attachView(this)
        showLoading()

        webview.loadUrl(resourceManager.getString(R.string.dami_dev_url))
        webview.settings.javaScriptEnabled = true
        webview.canGoBackOrForward(10)
        webview.settings.javaScriptCanOpenWindowsAutomatically = true
        webview.webViewClient = object : WebViewClient() {

            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                if (url != null && url.startsWith(resourceManager.getString(R.string.dami_dev_url), true)) {
                    return false
                } else {
                    showMessage(R.string.cannot_leave_page)
                }
                return true
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                if (loadingBar != null) {
                    hideLoading()
                }
                super.onPageFinished(view, url)
            }
        }


    }

    override fun onResume() {
        super.onResume()

        // when back button is pressed try to go to previous website
        webview?.isFocusableInTouchMode = true
        webview?.requestFocus()
        webview?.setOnKeyListener { _, keyCode, _ ->
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                if (webview.canGoBack()) {
                    webview.goBack()
                    showLoading()
                }
                return@setOnKeyListener true
            }
            false
        }
    }


    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
    }

}
