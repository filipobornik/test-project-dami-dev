package cz.filipobornik.testproject.ui.contactListFragment

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import cz.filipobornik.testproject.R
import cz.filipobornik.testproject.data.model.Contact
import cz.filipobornik.testproject.di.PerFragment
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.contact_item.view.*
import javax.inject.Inject

@PerFragment
class ContactAdapter @Inject constructor() : RecyclerView.Adapter<ContactAdapter.ViewHolder>() {

    /**
     * Filtered contact after user's search
     */
    private var filteredContacts: ArrayList<Contact> = arrayListOf()

    /**
     * All contacts without filtering
     */
    private var contacts: ArrayList<Contact> = arrayListOf()

    /**
     * Notifies when user item is clicked
     */
    private val itemOnClickObservable: PublishSubject<Contact> = PublishSubject.create()

    /**
     * Creates view holder for contact item
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.contact_item, parent, false)
        return ViewHolder(view)
    }

    /**
     * Get contacts count
     */
    override fun getItemCount(): Int {
        return filteredContacts.size
    }

    /**
     * Binds view holders views and specific contact values
     */
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.setOnClickListener({ itemOnClickObservable.onNext(filteredContacts[position]) })
        holder.bindContact(filteredContacts[position])
    }

    /**
     * Return item click observable that notifies when contact item is clicked
     */
    fun getItemOnClickObservable(): Observable<Contact> {
        return itemOnClickObservable
    }

    /**
     * Updates contact list
     */
    fun updateContacts(contacts: ArrayList<Contact>) {
        this.contacts = contacts
        this.contacts.sortBy { contact -> contact.name }
        this.filteredContacts = this.contacts
        notifyDataSetChanged()
    }

    /**
     * Search in contact list, displays results
     */
    fun searchContacts(query: String?) {
        val searchText = query?.toLowerCase()
        val searchWords = searchText?.split(" ")

        if (searchText == null || searchText.isEmpty() || searchWords == null) {
            filteredContacts = contacts
        } else {
            var filteredList: ArrayList<Contact> = ArrayList(contacts.toList())

            for (text in searchWords) {
                val tempList = filteredList.toList()
                for (contact in tempList) {
                    if (!contact.name.toString().toLowerCase().contains(text) && !contact.lastname.toString().toLowerCase().contains(text)
                            && !contact.email.toString().toLowerCase().contains(text)) {
                        filteredList.remove(contact)
                    }
                }
            }
            filteredContacts = filteredList
        }
        filteredContacts.sortBy { contact -> contact.name }
        notifyDataSetChanged()
    }

    override fun getItemId(position: Int): Long {
        return filteredContacts[position].id?.toLong() ?: 0
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        /**
         * Bind contact data to view
         */
        fun bindContact(contact: Contact) {
            itemView.txt_name.text = contact.name
            itemView.txtLastname.text = contact.lastname

            val options = RequestOptions()
                    .circleCrop()
                    .error(R.drawable.placeholder_image)

            Glide.with(itemView.context)
                    .load(contact.photo)
                    .apply(options)
                    .into(itemView.imgContact)
        }
    }

}