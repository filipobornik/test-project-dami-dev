package cz.filipobornik.testproject.ui.singInFragment

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.animation.AnimationUtils
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginResult
import cz.filipobornik.testproject.R
import cz.filipobornik.testproject.base.BaseFragment
import cz.filipobornik.testproject.ui.main.MainActivity
import cz.filipobornik.testproject.ui.registrationActivity.RegistrationActivity
import kotlinx.android.synthetic.main.fragment_sign_in.*
import javax.inject.Inject
import android.view.inputmethod.InputMethodManager


/**
 * Sign in fragment, handle user sign in or redirect user to registration
 */
class SignInFragment : BaseFragment(), SignInFragmentContract.View {

    @Inject
    lateinit var presenter: SignInFragmentContract.Presenter

    /**
     * Callback manager for facebook login
     */
    val callbackManager: CallbackManager = CallbackManager.Factory.create()

    /**
     * Start main activity and finish welcome activity
     */
    override fun startMainActivity() {
        val intent = Intent(activityContext, MainActivity::class.java)
        startActivity(intent)
        activity?.finish()
    }

    /**
     * Start registration activity
     */
    override fun startRegistrationActivity() {
        val intent = Intent(activityContext, RegistrationActivity::class.java)
        startActivity(intent)
    }

    /**
     * Show loading bar
     */
    override fun showLoading() {
        hideLoading()
        container.visibility = View.GONE
        loadingBar.visibility = View.VISIBLE
    }

    /**
     * Hide loading bar
     */
    override fun hideLoading() {
        loadingBar.visibility = View.GONE
        container.visibility = View.VISIBLE
    }


    private fun closeKeyboard() {
        val view = activity?.currentFocus
        if (view != null) {
            val imm = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    /**
     * Show no network connection indicator, on retry button click notifies presenter about it
     */
    override fun showNoNetworkConnectionIndicator() {
        val snackbar = Snackbar.make(activity!!.findViewById(android.R.id.content), R.string.no_network_connection, Snackbar.LENGTH_INDEFINITE)
        snackbar.setAction(resourceManager.getString(R.string.retry), { presenter.onRetryBtnClicked() })
        snackbar.show()
    }

    /**
     * Handle button sign in click and check inputs
     */
    override fun onBtnSignInClicked() {
        closeKeyboard()
        if (editTxtEmail.text.toString().isEmpty()) {
            inputLatoutEditTxtEmail.error = resourceManager.getString(R.string.empty_email)
            return
        }

        if (editTxtPassword.text.toString().isEmpty()) {
            inputLayoutEditTxtPassword.error = resourceManager.getString(R.string.empty_password)
            return
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(editTxtEmail.text.toString()).matches()) {
            inputLatoutEditTxtEmail.error = resourceManager.getString(R.string.email_not_valid)
            return
        }
        presenter.onBtnSignInClick(editTxtEmail.text.toString(), editTxtPassword.text.toString())
    }

    /**
     * Handle button register click, notifies presenter about it
     */
    override fun onBtnRegisterClicked() {
        presenter.onBtnRegisterClicked()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_sign_in, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.attachView(this)
        btnSignIn.setOnClickListener({ onBtnSignInClicked() })
        btnRegister.setOnClickListener({ onBtnRegisterClicked() })
        btnFbLogin.fragment = this
        btnFbLogin.run {
            setReadPermissions("email")
            registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
                override fun onSuccess(result: LoginResult?) {
                    if (result == null) {
                        showError(R.string.facebook_login_failed)
                        return
                    }
                    presenter.onBtnFacebookSignInClick(result)
                }

                override fun onCancel() {
                    showError(R.string.facebook_login_failed)
                }

                override fun onError(error: FacebookException?) {
                    showError(R.string.facebook_login_failed)
                }

            })
        }

        animateViewCreation()

    }

    private fun animateViewCreation() {
        val translateFrom = -300f
        val translateTo = 0f
        val alphaFrom = 0f
        val alphaTo = 1f
        val duration: Long = 650

        val logoTranslation = ObjectAnimator.ofFloat(imgDamiDevLogo, "translationY", translateFrom, translateTo)
        logoTranslation.duration = duration
        logoTranslation.interpolator = AccelerateDecelerateInterpolator()
        val logoFadeIn = ObjectAnimator.ofFloat(imgDamiDevLogo, "alpha", alphaFrom, alphaTo)
        logoFadeIn.duration = duration
        logoFadeIn.interpolator = AccelerateDecelerateInterpolator()


        val animatorSet = AnimatorSet()
        animatorSet.play(logoTranslation).with(logoFadeIn)
        animatorSet.start()

        val layoutAnimationController = AnimationUtils.loadLayoutAnimation(activityContext, R.anim.layout_animation_fall_down)
        container.layoutAnimation = layoutAnimationController
        container.scheduleLayoutAnimation()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        callbackManager.onActivityResult(requestCode, resultCode, data)
        super.onActivityResult(requestCode, resultCode, data)
    }


    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
    }
}
