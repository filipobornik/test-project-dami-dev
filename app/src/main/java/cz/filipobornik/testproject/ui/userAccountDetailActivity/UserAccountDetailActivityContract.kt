package cz.filipobornik.testproject.ui.userAccountDetailActivity

import cz.filipobornik.testproject.base.BaseMVPPresenter
import cz.filipobornik.testproject.base.BaseMVPView
import cz.filipobornik.testproject.data.model.User

abstract class UserAccountDetailActivityContract {

    interface View : BaseMVPView {

        fun startUserAccountEditActivity()

        fun showUserAccountDetails(user: User)

        fun onEditFABClicked()

    }

    interface Presenter : BaseMVPPresenter<View> {

        fun getUserAccountDetailData()

    }

}