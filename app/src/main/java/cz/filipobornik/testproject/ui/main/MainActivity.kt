package cz.filipobornik.testproject.ui.main

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.app.Fragment
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import com.crashlytics.android.Crashlytics
import cz.filipobornik.testproject.R
import cz.filipobornik.testproject.base.BaseActivity
import cz.filipobornik.testproject.data.model.User
import cz.filipobornik.testproject.ui.aboutAppFragment.AboutAppFragment
import cz.filipobornik.testproject.ui.addContactActivity.AddContactActivity
import cz.filipobornik.testproject.ui.contactListFragment.ContactListFragment
import cz.filipobornik.testproject.ui.mapFragment.MapFragment
import cz.filipobornik.testproject.ui.userAccountDetailActivity.UserAccountDetailActivity
import cz.filipobornik.testproject.ui.welcomeActivity.WelcomeActivity
import cz.filipobornik.testproject.utils.UserSessionManager
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import javax.inject.Inject
import cz.filipobornik.testproject.ui.tutorialActivity.TutorialActivity
import cz.filipobornik.testproject.ui.videoFragment.VideoFragment
import cz.filipobornik.testproject.ui.webviewFragment.WebviewFragment
import io.fabric.sdk.android.Fabric

/**
 * MainActivity which contains different fragments
 */
class MainActivity : BaseActivity(),
        NavigationView.OnNavigationItemSelectedListener,
        MainActivityContract.View,
        HasSupportFragmentInjector {

    @Inject
    lateinit var presenter: MainActivityContract.Presenter

    @Inject
    lateinit var fragmentInjector: DispatchingAndroidInjector<Fragment>

    @Inject
    lateinit var sessionManager: UserSessionManager

    /**
     * Represents which fragment should be displayed after navigation drawer item is clicked
     */
    private var selectedNavItemId: Int = 0

    private val KEY_SELECTED_NAV_ITEM_ID = "SelectedNavItemIdKey"

    /**
     * Navigation drawer item click, true when item clicked, false when drawer closed without item clicked
     */
    private var navigationItemClicked: Boolean = false

    /**
     * Represents currently logged user
     */
    private lateinit var loggedUser: User

    /**
     * Start Welcome activity
     */
    override fun startWelcomeActivity() {
        val intent = Intent(this, WelcomeActivity::class.java)
        startActivity(intent)
        finish()
    }

    /**
     * Start AddContactActivity for adding new contact
     */
    override fun startAddContactActivity() {
        val intent = Intent(this, AddContactActivity::class.java)
        startActivity(intent)
    }

    override fun startUserAccountDetailActivity() {
        val intent = Intent(this, UserAccountDetailActivity::class.java)
        startActivity(intent)
    }

    /**
     * Start TutorialActivity that displays tutorial about using the app to the user
     */
    override fun startTutorialActivity() {
        val intent = Intent(this, TutorialActivity::class.java)
        startActivity(intent)
        finish()
    }

    /**
     * Handle fab btn add click, start AddContactActivity
     */
    override fun onFabBtnAddClicked() {
        startAddContactActivity()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Fabric.with(this, Crashlytics())
        Crashlytics.log("Activity created")
        setSupportActionBar(toolbar)

        fab_add.setOnClickListener({ onFabBtnAddClicked() })

        presenter.attachView(this)
        presenter.checkLogin()

        // opens default fragment when activity is created or opens previously opened fragment when activity is recreated
        selectedNavItemId = savedInstanceState?.getInt(KEY_SELECTED_NAV_ITEM_ID) ?: R.id.nav_contacts

        navigationItemClicked = true
        handleNavigationItemSelected()
    }

    override fun onStart() {
        super.onStart()
        setUpNavBar()
    }

    /**
     * Set up navigation view
     */
    private fun setUpNavBar() {
        val toggle: ActionBarDrawerToggle = object : ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            override fun onDrawerClosed(drawerView: View) {
                super.onDrawerClosed(drawerView)
                onDrawerClosed()
            }
        }

        drawer_layout.addDrawerListener(toggle)
        toggle.onDrawerClosed(drawer_layout)
        toggle.syncState()
        nav_view.setCheckedItem(R.id.nav_contacts)
        nav_view.setNavigationItemSelectedListener(this)
        val navHeader = nav_view.getHeaderView(0)

        // if user is logged in add in nav header user's name and email
        if (sessionManager.isUserLoggedIn()) {
            loggedUser = sessionManager.getLoggedUser()
            val name: String = if (loggedUser.name == null) " " else loggedUser.name.toString()
            val lastname: String = if (loggedUser.lastname == null) " " else loggedUser.lastname.toString()
            val email: String = if (loggedUser.email == null) "je" else loggedUser.email.toString()

            val navHeaderEmail = navHeader.findViewById<TextView>(R.id.nav_header_email)
            val navHeaderName = navHeader.findViewById<TextView>(R.id.nav_header_name)

            navHeaderName.text = String.format(resourceManager.getString(R.string.nav_header_name), name, lastname)
            navHeaderEmail.text = email
        }

    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putInt(KEY_SELECTED_NAV_ITEM_ID, selectedNavItemId)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
    }

    /**
     * Close navigation drawer when back button is clicked
     */
    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    /**
     * Creates options menu
     */
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    /**
     * Handle navigation view item clicks
     */
    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.nav_account_details) {
            startUserAccountDetailActivity()
            return true
        }

        // if item that has been clicked is same as currently selected item don't register it as clicked
        navigationItemClicked = selectedNavItemId != item.itemId
        selectedNavItemId = item.itemId
        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    /**
     * Checks if item in navigation drawer was clicked before drawer was closed
     */
    override fun onDrawerClosed() {
        if (!navigationItemClicked) return
        handleNavigationItemSelected()
    }

    /**
     * Handles navigation item click and shows specific fragment
     */
    override fun handleNavigationItemSelected() {
        navigationItemClicked = false
        when (selectedNavItemId) {
            R.id.nav_about_app -> {
                addFragment(R.id.container, AboutAppFragment(), resourceManager.getString(R.string.about_app))
                fab_add.hide()
            }
            R.id.nav_log_out -> {
                presenter.navLogOutItemClicked()
            }
            R.id.nav_contacts -> {
                addFragment(R.id.container, ContactListFragment(), resourceManager.getString(R.string.contacts))
                fab_add.show()
            }
            R.id.nav_webview -> {
                addFragment(R.id.container, WebviewFragment(), resourceManager.getString(R.string.webview))
                fab_add.hide()
            }
            R.id.nav_account_details -> {
                startUserAccountDetailActivity()
            }
            R.id.nav_map -> {
                addFragment(R.id.container, MapFragment(), resources.getString(R.string.map))
                fab_add.hide()
            }
            R.id.nav_videos -> {
                addFragment(R.id.container, VideoFragment(), resourceManager.getString(R.string.videos))
                fab_add.hide()
            }
        }
    }

    override fun supportFragmentInjector(): AndroidInjector<Fragment> {
        return fragmentInjector
    }

}
