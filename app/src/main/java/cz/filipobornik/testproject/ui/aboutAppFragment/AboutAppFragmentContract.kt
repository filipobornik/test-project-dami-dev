package cz.filipobornik.testproject.ui.aboutAppFragment

import cz.filipobornik.testproject.base.BaseMVPPresenter
import cz.filipobornik.testproject.base.BaseMVPView

abstract class AboutAppFragmentContract {

    interface View : BaseMVPView

    interface Presenter : BaseMVPPresenter<View>

}