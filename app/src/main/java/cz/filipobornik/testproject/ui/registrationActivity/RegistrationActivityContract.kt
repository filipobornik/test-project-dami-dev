package cz.filipobornik.testproject.ui.registrationActivity

import cz.filipobornik.testproject.base.BaseMVPPresenter
import cz.filipobornik.testproject.base.BaseMVPView

abstract class RegistrationActivityContract {

    interface View : BaseMVPView {

        fun startMainActivity()

        fun onBtnRegisterClicked()

        fun showLoading()

        fun hideLoading()

        fun showNoNetworkConnectionIndicator()

    }

    interface Presenter : BaseMVPPresenter<View> {

        fun onBtnRegisterClicked(email: String, password: String)

        fun onRetryBtnClicked()

    }

}