package cz.filipobornik.testproject.ui.webviewFragment

import android.support.v4.app.Fragment
import cz.filipobornik.testproject.base.BaseFragmentModule
import cz.filipobornik.testproject.di.PerFragment
import dagger.Binds
import dagger.Module

@Module(includes = [BaseFragmentModule::class])
abstract class WebviewFragmentModule {

    @Binds
    @PerFragment
    abstract fun provideFragment(fragment: WebviewFragment): Fragment

    @Binds
    @PerFragment
    abstract fun providePresenter(presenter: WebviewFragmentPresenter): WebviewFragmentContract.Presenter

}