package cz.filipobornik.testproject.ui.userAccountDetailActivity

import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import cz.filipobornik.testproject.R
import cz.filipobornik.testproject.base.BaseActivity
import cz.filipobornik.testproject.data.model.User
import cz.filipobornik.testproject.databinding.ActivityUserAccountDetailBinding
import cz.filipobornik.testproject.ui.editUserAccountActivity.EditUserAccountActivity
import cz.filipobornik.testproject.utils.Utils
import kotlinx.android.synthetic.main.activity_user_account_detail.*
import javax.inject.Inject

class UserAccountDetailActivity : BaseActivity(), UserAccountDetailActivityContract.View {

    @Inject
    lateinit var presenter: UserAccountDetailActivityContract.Presenter

    private lateinit var binding: ActivityUserAccountDetailBinding

    /**
     * Show user account details
     */
    override fun showUserAccountDetails(user: User) {
        val emptyTxtViewIndicator = resourceManager.getString(R.string.empty_txt_view_indicator)
        binding.user = user
        txt_phone.text = if (user.phone == null) emptyTxtViewIndicator else Utils.formatPhoneNumber(user.phone)

        val fullName = "${user.name} ${user.lastname}"
        if (fullName.isEmpty()) {
            collapsing_toolbar_layout?.title = emptyTxtViewIndicator
        } else {
            collapsing_toolbar_layout?.title = fullName
        }

        val options = RequestOptions()
                .error(R.drawable.user_img_placeholder)
//                .diskCacheStrategy(DiskCacheStrategy.NONE)
//                .skipMemoryCache(true)
                .placeholder(R.drawable.user_img_placeholder)
                .signature(user.imageVersionSignature)

        Glide.with(this)
                .load(user.photo ?: "")
                .transition(DrawableTransitionOptions().crossFade())
                .apply(options)
                .into(img_user)
    }

    /**
     * Handle edit fab click
     */
    override fun onEditFABClicked() {
        startUserAccountEditActivity()
    }

    /**
     * Start UserAccountEditActivity
     */
    override fun startUserAccountEditActivity() {
        val intent = Intent(this, EditUserAccountActivity::class.java)
        startActivity(intent)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_account_detail)
        presenter.attachView(this)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_user_account_detail)
        presenter.getUserAccountDetailData()
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        fab_edit.setOnClickListener { onEditFABClicked() }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu. main, menu);
        return true
    }

    override fun onRestart() {
        super.onRestart()
        presenter.getUserAccountDetailData()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
    }
}
