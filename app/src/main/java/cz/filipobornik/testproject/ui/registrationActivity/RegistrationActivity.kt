package cz.filipobornik.testproject.ui.registrationActivity

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.util.Patterns
import android.view.View
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.animation.AnimationUtils
import cz.filipobornik.testproject.R
import cz.filipobornik.testproject.base.BaseActivity
import cz.filipobornik.testproject.ui.main.MainActivity
import kotlinx.android.synthetic.main.activity_register.*
import javax.inject.Inject

/**
 * Registration activity that allows user to register new account
 */
class RegistrationActivity : BaseActivity(), RegistrationActivityContract.View {

    @Inject
    lateinit var presenter: RegistrationActivityContract.Presenter

    /**
     * Starts MainActivity
     */
    override fun startMainActivity() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }

    /**
     * Show loading bar
     */
    override fun showLoading() {
        container.visibility = View.GONE
        loading_bar.visibility = View.VISIBLE
    }

    /**
     * Hide loading bar
     */
    override fun hideLoading() {
        loading_bar.visibility = View.GONE
        container.visibility = View.VISIBLE
    }

    /**
     * Handles btn register click and validate inputs
     */
    override fun onBtnRegisterClicked() {
        val email = editTxtEmail.text.toString()
        val password = editTxtPassword.text.toString()
        val passwordAgain = editTxtPasswordAgain.text.toString()

        if (editTxtEmail.text.toString().isEmpty()) {
            inputLayoutEditTxtEmail.error = resourceManager.getString(R.string.empty_email)
            return
        }

        if (editTxtPassword.text.toString().isEmpty()) {
            inputLayoutEditTxtPassword.error = resourceManager.getString(R.string.empty_password)
        }

        if (editTxtPasswordAgain.text.toString().isEmpty()) {
            inputLayoutEditTxtPassword.error = resourceManager.getString(R.string.empty_password)
        }

        if (password != passwordAgain) {
            inputLayoutEditTxtPassword.error = resourceManager.getString(R.string.password_dont_match)
            inputLayoutEditTxtPasswordAgain.error = resourceManager.getString(R.string.password_dont_match)
            return
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(editTxtEmail.text.toString()).matches()) {
            inputLayoutEditTxtEmail.error = resourceManager.getString(R.string.email_not_valid)
            return
        }

        presenter.onBtnRegisterClicked(email, password)
    }

    /**
     * Show no network connection indicator
     */
    override fun showNoNetworkConnectionIndicator() {
        val snackbar = Snackbar.make(findViewById(android.R.id.content), R.string.no_network_connection, Snackbar.LENGTH_INDEFINITE)
        snackbar.setAction(resourceManager.getString(R.string.retry), { presenter.onRetryBtnClicked() })
        snackbar.show()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        presenter.attachView(this)
        supportActionBar?.title = resourceManager.getString(R.string.registration)
        btnRegister.setOnClickListener({ onBtnRegisterClicked() })
        animateViewCreation()
    }

    private fun animateViewCreation() {
        val translateFrom = -300f
        val translateTo = 0f
        val alphaFrom = 0f
        val alphaTo = 1f
        val duration: Long = 650

        val logoTranslation = ObjectAnimator.ofFloat(imgDamiDevLogo, "translationY", translateFrom, translateTo)
        logoTranslation.duration = duration
        logoTranslation.interpolator = AccelerateDecelerateInterpolator()
        val logoFadeIn = ObjectAnimator.ofFloat(imgDamiDevLogo, "alpha", alphaFrom, alphaTo)
        logoFadeIn.duration = duration
        logoFadeIn.interpolator = AccelerateDecelerateInterpolator()


        val animatorSet = AnimatorSet()
        animatorSet.play(logoTranslation).with(logoFadeIn)
        animatorSet.start()

        val layoutAnimationController = AnimationUtils.loadLayoutAnimation(this, R.anim.layout_animation_fall_down)
        content.layoutAnimation = layoutAnimationController
        content.scheduleLayoutAnimation()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
    }
}
