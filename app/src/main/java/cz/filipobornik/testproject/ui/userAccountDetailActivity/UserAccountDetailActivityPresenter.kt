package cz.filipobornik.testproject.ui.userAccountDetailActivity

import cz.filipobornik.testproject.base.BasePresenter
import cz.filipobornik.testproject.di.PerActivity
import cz.filipobornik.testproject.utils.UserSessionManager
import javax.inject.Inject

@PerActivity
class UserAccountDetailActivityPresenter @Inject constructor(
        private val userSessionManager: UserSessionManager
): BasePresenter<UserAccountDetailActivityContract.View>(), UserAccountDetailActivityContract.Presenter {

    /**
     * Get user account details from user session manager and notify view to display it
     */
    override fun getUserAccountDetailData() {
        view?.showUserAccountDetails(userSessionManager.getLoggedUser())
    }

}

