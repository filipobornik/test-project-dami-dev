package cz.filipobornik.testproject.ui.videoFragment

import android.support.v4.app.Fragment
import cz.filipobornik.testproject.base.BaseFragmentModule
import cz.filipobornik.testproject.di.PerFragment
import dagger.Binds
import dagger.Module

@Module(includes = [BaseFragmentModule::class])
abstract class VideoFragmentModule {

    @Binds
    @PerFragment
    abstract fun provideFragment(videoFragment: VideoFragment): Fragment

    @Binds
    @PerFragment
    abstract fun providePresenter(presenter: VideoFragmentPresenter): VideoFragmentContract.Presenter

}