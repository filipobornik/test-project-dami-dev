package cz.filipobornik.testproject.ui.welcomeActivity

import android.os.Bundle
import android.support.annotation.AnimRes
import android.support.annotation.IdRes
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import cz.filipobornik.testproject.R
import cz.filipobornik.testproject.base.BaseActivity
import cz.filipobornik.testproject.ui.aboutAppFragment.AboutAppFragment
import cz.filipobornik.testproject.ui.singInFragment.SignInFragment
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.activity_welcome.*
import javax.inject.Inject

class WelcomeActivity : BaseActivity(), WelcomeActivityContract.View, HasSupportFragmentInjector {

    @Inject
    lateinit var presenter: WelcomeActivityContract.Presenter

    @Inject
    lateinit var fragmentInjector: DispatchingAndroidInjector<Fragment>

    private lateinit var fragments: Array<Fragment>

    private var currentlyDisplaydFragmentId: Int = 0

    /**
     * Listen for item click in bottom navigation, show specific fragment
     */
    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        if (currentlyDisplaydFragmentId != item.itemId) {
            currentlyDisplaydFragmentId = item.itemId
            when (item.itemId) {
                R.id.navigation_account -> {
                    addFragment(container.id, fragments[0], R.anim.slide_in_left, R.anim.slide_out_right, resourceManager.getString(R.string.title_sign_in))
                    return@OnNavigationItemSelectedListener true
                }
                R.id.navigation_about_app -> {
                    addFragment(container.id, fragments[1], R.anim.slide_in_right, R.anim.slide_out_left, resourceManager.getString(R.string.title_about_app))
                    return@OnNavigationItemSelectedListener true
                }
            }
        }
        false
    }

    /**
     * Add fragment with custom animation
     */
    fun addFragment(id: Int, fragment: Fragment, @AnimRes inAnim: Int, @AnimRes outAnim: Int, toolbarTitle: String) {
        fragmentManager.beginTransaction()
                .setCustomAnimations(inAnim, outAnim)
                .replace(id, fragment)
                .commit()
        supportActionBar?.title = toolbarTitle
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome)
        supportActionBar?.setDisplayHomeAsUpEnabled(false)
        presenter.attachView(this)
        fragments = arrayOf(SignInFragment(), AboutAppFragment())
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        addFragment(container.id, SignInFragment(), resourceManager.getString(R.string.title_sign_in))
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
    }

    override fun supportFragmentInjector(): AndroidInjector<Fragment> {
        return fragmentInjector
    }
}
