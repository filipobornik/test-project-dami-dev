package cz.filipobornik.testproject.ui.singInFragment

import android.support.v4.app.Fragment
import cz.filipobornik.testproject.base.BaseFragmentModule
import cz.filipobornik.testproject.di.PerFragment
import dagger.Binds
import dagger.Module

@Module(includes = [BaseFragmentModule::class])
abstract class SignInFragmentModule {

    @Binds
    @PerFragment
    abstract fun provideFragment(fragment: SignInFragment): Fragment

    @Binds
    @PerFragment
    abstract fun providePresenter(presenter: SignInFragmentPresenter): SignInFragmentContract.Presenter

}