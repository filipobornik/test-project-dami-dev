package cz.filipobornik.testproject.ui.tutorialActivity

import android.content.Context
import android.content.res.Resources
import android.support.v4.view.PagerAdapter
import android.view.View
import android.view.ViewGroup
import cz.filipobornik.testproject.R
import cz.filipobornik.testproject.di.ActivityContext
import javax.inject.Inject
import android.view.LayoutInflater



class TutorialViewpagerAdapter @Inject constructor(private val resourceManager: Resources, @ActivityContext val context: Context) : PagerAdapter() {

    private val layouts = arrayListOf(
            R.layout.item_tutorial_about,
            R.layout.item_tutorial_made_with_love,
            R.layout.item_tutorial_coffe,
            R.layout.item_tutorial_help
    )

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return `object` == view
    }

    override fun getCount(): Int {
        return 4
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {

        val layoutInflater = LayoutInflater.from(context)
        val layout = layoutInflater.inflate(layouts[position], container, false)
        container.addView(layout)
        return layout

    }

    override fun destroyItem(collection: ViewGroup, position: Int, view: Any) {
        collection.removeView(view as View)
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return (position+1).toString()
    }
}