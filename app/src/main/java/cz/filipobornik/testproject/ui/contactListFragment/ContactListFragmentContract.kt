package cz.filipobornik.testproject.ui.contactListFragment

import cz.filipobornik.testproject.base.BaseMVPPresenter
import cz.filipobornik.testproject.base.BaseMVPView
import cz.filipobornik.testproject.data.model.Contact

abstract class ContactListFragmentContract {

    interface View : BaseMVPView {

        fun startContactDetailActivity(contact: Contact)

        fun showContacts(contacts: ArrayList<Contact>)

        fun onContactClicked(contact: Contact)

        fun hideLoading()

        fun showLoading()

        fun showNoNetworkConnectionIndicator()

        fun onSwipeRefresh()

    }

    interface Presenter : BaseMVPPresenter<View> {

        fun loadContacts(forceUpdate: Boolean)

        fun onRetryBtnClicked()

    }

}