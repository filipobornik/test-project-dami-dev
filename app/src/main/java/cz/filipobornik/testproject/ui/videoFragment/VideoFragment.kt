package cz.filipobornik.testproject.ui.videoFragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.youtube.player.YouTubeInitializationResult
import com.google.android.youtube.player.YouTubePlayer
import com.google.android.youtube.player.YouTubePlayerSupportFragment
import cz.filipobornik.testproject.R
import cz.filipobornik.testproject.base.BaseFragment
import cz.filipobornik.testproject.utils.Constants

/**
 * Video fragment that displays YouTube video
 */
class VideoFragment : BaseFragment(), YouTubePlayer.OnInitializedListener {

    private val RECOVERY_REQUEST = 1

    private lateinit var youTubePlayer: YouTubePlayer

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_video, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val youtubePlayerFragment = YouTubePlayerSupportFragment.newInstance()
        childFragmentManager
                .beginTransaction()
                .add(R.id.youtubesupportfragment, youtubePlayerFragment)
                .commit()

        youtubePlayerFragment.initialize(Constants.YOUTUBE_API_KEY, this)
    }

    override fun onInitializationSuccess(provider: YouTubePlayer.Provider?, player: YouTubePlayer?, wasRestored: Boolean) {
        if (!wasRestored) {
            this.youTubePlayer = player!!
            youTubePlayer.setPlayerStyle(YouTubePlayer.PlayerStyle.DEFAULT)
            youTubePlayer.cueVideo("XaNJzYBsnik")
        }
    }

    override fun onInitializationFailure(provider: YouTubePlayer.Provider?, errorReason: YouTubeInitializationResult?) {
        if (errorReason?.isUserRecoverableError == true) {
            errorReason.getErrorDialog(activity, RECOVERY_REQUEST)?.show()
        } else {
            val errorMessage = "${resourceManager.getString(R.string.player_error)} ${errorReason.toString()}"
            showError(errorMessage)
        }
    }

}
