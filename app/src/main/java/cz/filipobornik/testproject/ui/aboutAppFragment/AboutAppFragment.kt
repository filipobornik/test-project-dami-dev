package cz.filipobornik.testproject.ui.aboutAppFragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import cz.filipobornik.testproject.R
import cz.filipobornik.testproject.base.BaseFragment
import javax.inject.Inject

/**
 * About app fragment shows basic info about the app and developer
 */
class AboutAppFragment : BaseFragment(), AboutAppFragmentContract.View {

    @Inject lateinit var presenter: AboutAppFragmentContract.Presenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_about_app, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.attachView(this)
        activity?.actionBar?.title = resourceManager.getString(R.string.about_app)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
    }
}
