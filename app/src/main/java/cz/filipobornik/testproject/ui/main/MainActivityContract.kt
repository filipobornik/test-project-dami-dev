package cz.filipobornik.testproject.ui.main

import cz.filipobornik.testproject.base.BaseMVPPresenter
import cz.filipobornik.testproject.base.BaseMVPView

abstract class MainActivityContract {

    interface View : BaseMVPView {

        fun startWelcomeActivity()

        fun startAddContactActivity()

        fun startUserAccountDetailActivity()

        fun startTutorialActivity()

        fun onDrawerClosed()

        fun handleNavigationItemSelected()

        fun onFabBtnAddClicked()

    }

    interface Presenter : BaseMVPPresenter<View> {

        fun checkLogin()

        fun navLogOutItemClicked()

    }

}