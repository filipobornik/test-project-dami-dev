package cz.filipobornik.testproject.ui.mapFragment

import cz.filipobornik.testproject.base.BasePresenter
import cz.filipobornik.testproject.di.PerFragment
import javax.inject.Inject

@PerFragment
class MapFragmentPresenter @Inject constructor() : BasePresenter<MapFragmentContract.View>(), MapFragmentContract.Presenter {

}