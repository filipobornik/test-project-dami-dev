package cz.filipobornik.testproject.ui.contactDetailActivity

import android.support.v7.app.AppCompatActivity
import cz.filipobornik.testproject.base.BaseActivityModule
import cz.filipobornik.testproject.di.PerActivity
import dagger.Binds
import dagger.Module

@Module(includes = [BaseActivityModule::class])
abstract class ContactDetailActivityModule {

    @Binds
    @PerActivity
    abstract fun provideActivity(activity: ContactDetailActivity): AppCompatActivity

    @Binds
    @PerActivity
    abstract fun providePresenter(presenter: ContactDetailActivityPresenter): ContactDetailActivityContract.Presenter

}