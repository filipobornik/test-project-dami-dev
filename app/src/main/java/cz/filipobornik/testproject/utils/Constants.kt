package cz.filipobornik.testproject.utils

/**
 * Constants used in app
 */
class Constants {

    companion object {

        const val BASE_URL = "https://androidtest.damidev.com/api/"

        const val EXTRA_CONTACT = "extra_contact"

        const val YOUTUBE_API_KEY = "AIzaSyCxQVsF2yojagnlZGCUTYVHkFG40Z4hARw"

    }
}