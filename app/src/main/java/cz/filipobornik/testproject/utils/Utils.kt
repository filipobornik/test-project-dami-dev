package cz.filipobornik.testproject.utils

import android.os.Build
import android.telephony.PhoneNumberUtils

class Utils {

    companion object {
        /**
         * Format phone number into *** *** ***
         * If phone number has different number of digits return unformatted phone number
         * If phone number is null return empty string
         */
        fun formatPhoneNumber(unformattedPhone: String?): String {
            if (unformattedPhone == null) {
                return ""
            }
            if (unformattedPhone.length != 9) {
                return unformattedPhone
            }
            else {
                return "${unformattedPhone.substring(0,3)} ${unformattedPhone.substring(3,6)} ${unformattedPhone.substring(6,9)}"
            }
        }

        /**
         * Check, if phone number has 9 digits
         */
        fun isPhoneNumberValid(phoneNumber: String): Boolean {
            if (!phoneNumber.matches("[0-9]{9}".toRegex())) {
                return false
            }
            return true
        }
    }

}