package cz.filipobornik.testproject.utils.schedulers

import io.reactivex.Scheduler

interface BaseSchedulersProvider {

    fun computation(): Scheduler

    fun io(): Scheduler

    fun ui(): Scheduler

}