package cz.filipobornik.testproject.utils

import com.bumptech.glide.load.Key
import java.nio.ByteBuffer
import java.security.MessageDigest

/**
 * Represents version of user/contact image added by user
 * When user updates image, the image url on server remains the same
 * This signature is changed whenever the image is changed
 * This notifies glide that the image on the same url has changed
 */
class ImageVersionSignature : Key {

    /**
     * Represents current version of the image which is get as current time in miliseconds
     */
    private var currentVersion: Long = 0

    constructor() {
        currentVersion = System.currentTimeMillis()
    }

    constructor(currentVersion: Long) {
        this.currentVersion = currentVersion
    }

    /**
     * Returns current version
     */
    fun getCurrentVersion(): Long = currentVersion

    override fun equals(other: Any?): Boolean {
        if (other is ImageVersionSignature) {
            val other = other
            return currentVersion == other.currentVersion
        }
        return false
    }

    override fun hashCode(): Int {
        return currentVersion.toInt()
    }

    /**
     * Increase current time by getting current time in miliseconds
     */
    fun increaseCurrentVersion() {
        currentVersion = System.currentTimeMillis()
    }

    override fun updateDiskCacheKey(messageDigest: MessageDigest) {
        messageDigest.update(ByteBuffer.allocate(Integer.SIZE).putLong(currentVersion).array())
    }
}