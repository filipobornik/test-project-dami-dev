package cz.filipobornik.testproject.utils

import android.content.res.Resources
import android.util.Log
import com.google.gson.Gson
import cz.filipobornik.testproject.R
import cz.filipobornik.testproject.data.model.Response
import retrofit2.HttpException
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ErrorUtils @Inject constructor(
        private val resourceManager: Resources,
        private val gson: Gson) {

    /**
     * Check if session token is still active
     */
    fun isSessionTokenActive(error: Throwable): Boolean {
        if (error !is HttpException) {
            return true
        }
        val errorBodyJSON = error.response().errorBody()?.string()
        val responseCode = gson.fromJson(errorBodyJSON, Response::class.java).responseCode
        return responseCode != 6
    }

    /**
     * Returns error message from error
     */
    fun returnApiErrorMessage(error: Throwable): String {
        // Error is HTTP Exception
        if (error is HttpException) {
            val errorBodyJSON = error.response().errorBody()?.string()
            val responseCode = gson.fromJson(errorBodyJSON, Response::class.java).responseCode
            val message = gson.fromJson(errorBodyJSON, Response::class.java).responseCodeText

            if (responseCode == 7) {
                return resourceManager.getString(R.string.bad_email_or_password)
            }

            if (responseCode == 6) {
                return resourceManager.getString(R.string.token_expired)
            }

            if(responseCode == 8) {
                return resourceManager.getString(R.string.email_is_already_used)
            }

            return resourceManager.getString(R.string.some_api_error_occured) + responseCode + message
        }
        Log.d("imageError", error.localizedMessage)
        return resourceManager.getString(R.string.some_api_error_occured) + error.localizedMessage
    }

}