package cz.filipobornik.testproject.utils

import android.content.SharedPreferences
import android.content.res.Resources
import com.facebook.login.LoginManager
import cz.filipobornik.testproject.R
import cz.filipobornik.testproject.data.model.User
import io.realm.Realm
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UserSessionManager @Inject constructor(private val sharedPreferences: SharedPreferences, private val resourceManager: Resources) {

    /**
     * check if user is logged in
     */
    fun isUserLoggedIn(): Boolean {
        return sharedPreferences.getBoolean(resourceManager.getString(R.string.is_user_logged_id), false)
    }

    /**
     * handle login, add user data into shared preferences
     */
    fun handleLogin(user: User) {
        val sharedPrefsEdit = sharedPreferences.edit()
        sharedPrefsEdit.putBoolean(resourceManager.getString(R.string.is_user_logged_id), true)
        sharedPrefsEdit.putInt(resourceManager.getString(R.string.shared_prefs_user_id), user.id)
        sharedPrefsEdit.putString(resourceManager.getString(R.string.shared_prefs_user_email), user.email)
        sharedPrefsEdit.putString(resourceManager.getString(R.string.shared_prefs_user_description), user.description)
        sharedPrefsEdit.putString(resourceManager.getString(R.string.shared_prefs_user_fID), user.fID)
        sharedPrefsEdit.putString(resourceManager.getString(R.string.shared_prefs_user_lastname), user.lastname)
        sharedPrefsEdit.putString(resourceManager.getString(R.string.shared_prefs_user_name), user.name)
        sharedPrefsEdit.putString(resourceManager.getString(R.string.shared_prefs_user_phone), user.phone)
        sharedPrefsEdit.putString(resourceManager.getString(R.string.shared_prefs_user_photo), user.photo)
        sharedPrefsEdit.putString(resourceManager.getString(R.string.shared_prefs_user_token), user.token)
        sharedPrefsEdit.putLong(resourceManager.getString(R.string.shared_prefs_image_version), user.imageVersionSignature.getCurrentVersion())
        sharedPrefsEdit.apply()
    }

    /**
     * Update user info in shared preferences
     */
    fun updateUser(user: User) {
        val sharedPrefsEdit = sharedPreferences.edit()
        sharedPrefsEdit.putString(resourceManager.getString(R.string.shared_prefs_user_email), user.email)
        sharedPrefsEdit.putString(resourceManager.getString(R.string.shared_prefs_user_description), user.description)
        sharedPrefsEdit.putString(resourceManager.getString(R.string.shared_prefs_user_fID), user.fID)
        sharedPrefsEdit.putString(resourceManager.getString(R.string.shared_prefs_user_lastname), user.lastname)
        sharedPrefsEdit.putString(resourceManager.getString(R.string.shared_prefs_user_name), user.name)
        sharedPrefsEdit.putString(resourceManager.getString(R.string.shared_prefs_user_phone), user.phone)
        sharedPrefsEdit.putString(resourceManager.getString(R.string.shared_prefs_user_photo), user.photo)
        sharedPrefsEdit.putLong(resourceManager.getString(R.string.shared_prefs_image_version), user.imageVersionSignature.getCurrentVersion())
        sharedPrefsEdit.apply()
    }

    /**
     * returns logged user object, if user is not logged in return empty object with user id -1
     */
    fun getLoggedUser(): User {
        return if (!isUserLoggedIn()) {
            User(-1, null, null, null, null, null, null, null, "", ImageVersionSignature())
        } else {
            User(
                    sharedPreferences.getInt(resourceManager.getString(R.string.shared_prefs_user_id), -1),
                    sharedPreferences.getString(resourceManager.getString(R.string.shared_prefs_user_name), null),
                    sharedPreferences.getString(resourceManager.getString(R.string.shared_prefs_user_lastname), null),
                    sharedPreferences.getString(resourceManager.getString(R.string.shared_prefs_user_email), ""),
                    sharedPreferences.getString(resourceManager.getString(R.string.shared_prefs_user_photo), null),
                    sharedPreferences.getString(resourceManager.getString(R.string.shared_prefs_user_fID), null),
                    sharedPreferences.getString(resourceManager.getString(R.string.shared_prefs_user_description), null),
                    sharedPreferences.getString(resourceManager.getString(R.string.shared_prefs_user_phone), null),
                    sharedPreferences.getString(resourceManager.getString(R.string.shared_prefs_user_token), ""),
                    ImageVersionSignature(sharedPreferences.getLong(resourceManager.getString(R.string.shared_prefs_image_version), 0)))
        }
    }

    /**
     * Handles log out, delete user data from shared preferences
     */
    fun handleLogOut() {
        if (isUserLoggedIn()) {
            val lastUserImageVersion = getLoggedUser().imageVersionSignature.getCurrentVersion()
            val sharedPrefsEdit = sharedPreferences.edit()
            sharedPrefsEdit.clear()
            sharedPrefsEdit.putBoolean(resourceManager.getString(R.string.is_user_logged_id), false)
            sharedPrefsEdit.putLong(resourceManager.getString(R.string.shared_prefs_last_user_image_version), lastUserImageVersion)
            sharedPrefsEdit.apply()

            val realm = Realm.getDefaultInstance()
            realm.executeTransaction { realm ->
                run {
                    realm.deleteAll()
                }
            }
            realm.close()
        }

        // Facebook logout
        LoginManager.getInstance().logOut()
    }

    /**
     * Get user session token
     */
    fun getUserToken(): String {
        return sharedPreferences.getString(resourceManager.getString(R.string.shared_prefs_user_token), "")
    }
}