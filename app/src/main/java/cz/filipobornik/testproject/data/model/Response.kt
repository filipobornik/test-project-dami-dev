package cz.filipobornik.testproject.data.model
import com.google.gson.annotations.SerializedName



data class Response<T>(
		@SerializedName("responseCode") val responseCode: Int?,
		@SerializedName("responseCodeText") val responseCodeText: String?,
		@SerializedName("response") val response: T?
)