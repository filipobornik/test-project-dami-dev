package cz.filipobornik.testproject.data.model
import com.google.gson.annotations.SerializedName
import cz.filipobornik.testproject.utils.ImageVersionSignature

data class User(
		@SerializedName("id") var id: Int,
		@SerializedName("name") var name: String?,
		@SerializedName("lastname") var lastname: String?,
		@SerializedName("email") var email: String?,
		@SerializedName("photo") var photo: String? = null,
		@SerializedName("fID") var fID: String? = null,
		@SerializedName("description") var description: String? = null,
		@SerializedName("phone") var phone: String? = null,
		@SerializedName("token") var token: String = "",
		var imageVersionSignature: ImageVersionSignature = ImageVersionSignature()
)