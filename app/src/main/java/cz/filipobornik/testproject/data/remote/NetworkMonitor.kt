package cz.filipobornik.testproject.data.remote

import android.content.Context
import android.net.ConnectivityManager
import cz.filipobornik.testproject.di.ApplicationContext
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Monitor network status
 */
@Singleton
class NetworkMonitor @Inject
constructor(@param:ApplicationContext private val context: Context) {

    /**
     * Check if phone is connected to the internet
     */
    val isConnected: Boolean
        get() {
            val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val networkInfo = connectivityManager.activeNetworkInfo
            return networkInfo != null && networkInfo.isConnectedOrConnecting
        }
}
