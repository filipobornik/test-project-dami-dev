package cz.filipobornik.testproject.data.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey


open class Contact() : RealmObject(), Parcelable {

    constructor(id: Int?, email: String?, name: String?, lastname: String?, description: String?, photo: String?, phone: String?) : this() {
        this.id = id
        this.email = email
        this.name = name
        this.lastname = lastname
        this.description = description
        this.photo = photo
        this.phone = phone
    }

    @PrimaryKey
    @SerializedName("id")
    @Expose
    var id: Int? = null

    @SerializedName("email")
    @Expose
    var email: String? = null

    @SerializedName("name")
    @Expose
    var name: String? = null

    @SerializedName("lastname")
    @Expose
    var lastname: String? = null

    @SerializedName("description")
    @Expose
    var description: String? = null

    @SerializedName("photo")
    @Expose
    var photo: String? = null

    @SerializedName("phone")
    @Expose
    var phone: String? = null

    constructor(parcel: Parcel) : this() {
        id = parcel.readValue(Int::class.java.classLoader) as? Int
        email = parcel.readString()
        name = parcel.readString()
        lastname = parcel.readString()
        description = parcel.readString()
        photo = parcel.readString()
        phone = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(id)
        parcel.writeString(email)
        parcel.writeString(name)
        parcel.writeString(lastname)
        parcel.writeString(description)
        parcel.writeString(photo)
        parcel.writeString(phone)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object {
        @JvmField
        val CREATOR = object : Parcelable.Creator<Contact> {
            override fun createFromParcel(parcel: Parcel): Contact {
                return Contact(parcel)
            }

            override fun newArray(size: Int): Array<Contact?> {
                return arrayOfNulls(size)
            }
        }
    }

}