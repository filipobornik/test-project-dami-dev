package cz.filipobornik.testproject.data.remote

import android.graphics.Bitmap
import cz.filipobornik.testproject.data.model.Contact
import cz.filipobornik.testproject.data.model.Response
import cz.filipobornik.testproject.data.model.User
import io.reactivex.Flowable
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.*

/**
 * Retrofit interface for api calls
 */
interface DamiDevService {

    @POST("login")
    @FormUrlEncoded
    fun login(@Field("email") email: String, @Field("password") password: String): Flowable<Response<User>>

    @POST("login")
    @FormUrlEncoded
    fun loginByFacebook(@Field("email") email: String, @Field("fID") fId: String): Flowable<Response<User>>

    @POST("register")
    @FormUrlEncoded
    fun register(@Field("email") email: String, @Field("password") password: String): Flowable<Response<User>>

    @POST("register")
    @FormUrlEncoded
    fun registerByFacebook(@Field("email") email: String, @Field("fID") fId: String): Flowable<Response<User>>

    @POST("getContacts")
    @FormUrlEncoded
    fun getContacts(@Field("token") token: String): Flowable<Response<ArrayList<Contact>>>

    @POST(value = "addContact")
    @Multipart
    fun addContact(@Part("token") token: RequestBody, @Part("name") name: RequestBody? = null, @Part("lastname") lastname: RequestBody? = null,
                   @Part("email") email: RequestBody? = null, @Part("phone") phone: RequestBody? = null,
                   @Part("description") description: RequestBody? = null, @Part photo: MultipartBody.Part? = null): Flowable<Response<Contact>>

    @POST(value = "updateAccount")
    @Multipart
    fun updateAccount(@Part("token") token: RequestBody, @Part("name") name: RequestBody? = null, @Part("lastname") lastname: RequestBody? = null,
                   @Part("email") email: RequestBody? = null, @Part("phone") phone: RequestBody? = null,
                   @Part("description") description: RequestBody? = null, @Part photo: MultipartBody.Part? = null): Flowable<Response<User>>

}