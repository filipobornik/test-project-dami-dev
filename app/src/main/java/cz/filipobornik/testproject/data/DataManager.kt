package cz.filipobornik.testproject.data

import cz.filipobornik.testproject.data.model.Contact
import cz.filipobornik.testproject.data.model.Response
import cz.filipobornik.testproject.data.model.User
import cz.filipobornik.testproject.data.remote.DamiDevService
import cz.filipobornik.testproject.utils.UserSessionManager
import cz.filipobornik.testproject.utils.schedulers.SchedulersProvider
import io.reactivex.Flowable
import io.realm.Realm
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File
import javax.inject.Inject

/**
 * Data manager object handles all operations with data
 */
class DataManager @Inject() constructor(
        private val damiDevService: DamiDevService,
        private val schedulersProvider: SchedulersProvider,
        private val userSessionManager: UserSessionManager) {

    /**
     * Sing in user, return flowable with user object
     */
    fun signIn(email: String, password: String): Flowable<User?> {
        return damiDevService.login(email, password)
                .subscribeOn(schedulersProvider.io())
                .flatMap { t -> Flowable.just(t.response) }
                .observeOn(schedulersProvider.ui())
    }

    /**
     * Sign in user by facebook, return flowable with user object
     */
    fun fbSignIn(email: String, fId: String): Flowable<User?> {
        return damiDevService.loginByFacebook(email, fId)
                .subscribeOn(schedulersProvider.io())
                .flatMap { t -> Flowable.just(t.response) }
                .observeOn(schedulersProvider.ui())
    }

    /**
     * Register new user, return flowable with user object
     */
    fun register(email: String, password: String): Flowable<User?> {
        return damiDevService.register(email, password)
                .subscribeOn(schedulersProvider.io())
                .flatMap { t -> Flowable.just(t.response) }
                .observeOn(schedulersProvider.ui())

    }

    /**
     * Register new user with facebook accout, return flowable with user object
     */
    fun fbRegister(email: String, fId: String): Flowable<User?> {
        return damiDevService.registerByFacebook(email, fId)
                .subscribeOn(schedulersProvider.io())
                .flatMap { t -> Flowable.just(t.response) }
                .observeOn(schedulersProvider.ui())
    }

    /**
     * Sync all user's contacts with api, return Flowable with Response that contains array of users as response attribute
     */
    fun syncContacts(): Flowable<ArrayList<Contact>?> {
        return damiDevService.getContacts(userSessionManager.getUserToken())
                .subscribeOn(schedulersProvider.io())
                .map {
                    val realm = Realm.getDefaultInstance()
                    realm.executeTransaction { realm ->
                        if (it.response != null) {
                            realm.insertOrUpdate(it.response)
                        }
                    }
                    realm.close()
                    return@map it.response
                }
                .observeOn(schedulersProvider.ui())
    }

    /**
     * Get all user's contacts from local database
     */
    fun getContacts(): List<Contact> {
        val realm = Realm.getDefaultInstance()
        val query = realm.where(Contact::class.java)
        val result = query.findAll()
        val contacts = realm.copyFromRealm(result)
        realm.close()
        return contacts
    }

    /**
     * Add new user contact, return Flowable with added contact
     */
    fun addContact(contact: Contact, photoFile: File?): Flowable<Response<Contact>> {
        val token = RequestBody.create(MediaType.parse("text/plain"), userSessionManager.getUserToken())
        val name = RequestBody.create(MediaType.parse("text/plain"), contact.name.toString())
        val lastname = RequestBody.create(MediaType.parse("text/plain"), contact.lastname.toString())
        val email = RequestBody.create(MediaType.parse("text/plain"), contact.email.toString())
        val phone = RequestBody.create(MediaType.parse("text/plain"), contact.phone.toString())
        val description = RequestBody.create(MediaType.parse("text/plain"), contact.description.toString())
        var body: MultipartBody.Part? = null
        if (photoFile != null) {
            val reqFile = RequestBody.create(MediaType.parse("image/*"), photoFile)
            body = MultipartBody.Part.createFormData("photo", photoFile.name, reqFile)
        }

        return damiDevService.addContact(token, name, lastname, email, phone, description, body)
                .subscribeOn(schedulersProvider.io())
                .observeOn(schedulersProvider.ui())
    }

    /**
     * Update user account information, return Flowable with updated contact
     */
    fun updateAccount(user: User, photoFile: File?): Flowable<Response<User>> {
        val token = RequestBody.create(MediaType.parse("text/plain"), userSessionManager.getUserToken())
        val name = RequestBody.create(MediaType.parse("text/plain"), user.name.toString())
        val lastname = RequestBody.create(MediaType.parse("text/plain"), user.lastname.toString())
        val email = RequestBody.create(MediaType.parse("text/plain"), user.email.toString())
        val phone = RequestBody.create(MediaType.parse("text/plain"), user.phone.toString())
        val description = RequestBody.create(MediaType.parse("text/plain"), user.description.toString())
        var body: MultipartBody.Part? = null
        if (photoFile != null) {
            val reqFile = RequestBody.create(MediaType.parse("image/*"), photoFile)
            body = MultipartBody.Part.createFormData("photo", photoFile.name, reqFile)
        }

        return damiDevService.updateAccount(token, name, lastname, email, phone, description, body)
                .subscribeOn(schedulersProvider.io())
                .observeOn(schedulersProvider.ui())
    }

}