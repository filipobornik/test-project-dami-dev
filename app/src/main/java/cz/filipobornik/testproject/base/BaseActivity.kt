package cz.filipobornik.testproject.base

import android.content.Intent
import android.content.res.Resources
import android.os.Bundle
import android.support.annotation.IdRes
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.google.firebase.analytics.FirebaseAnalytics
import dagger.android.AndroidInjection
import javax.inject.Inject
import javax.inject.Named
import cz.filipobornik.testproject.R
import cz.filipobornik.testproject.ui.main.MainActivity
import cz.filipobornik.testproject.ui.welcomeActivity.WelcomeActivity
import cz.filipobornik.testproject.utils.UserSessionManager

/**
 * Base activity every activity has to extend from
 */
open class BaseActivity : AppCompatActivity(), BaseMVPView {

    /**
     * Resource manager for accessing resources
     */
    @Inject
    lateinit var resourceManager: Resources

    /**
     * Fragment manager for inserting fragments into Activity
     */
    @Inject
    @field:Named(BaseActivityModule.ACTIVITY_FRAGMENT_MANAGER)
    lateinit var fragmentManager: FragmentManager

    /**
     * Session manager take care about logged user session
     */
    @Inject
    lateinit var userSessionManager: UserSessionManager

    /**
     * Firebase analytics to log events
     */
    @Inject
    lateinit var firebaseAnalytics: FirebaseAnalytics

    /**
     * Inject activity in Dagger dependency graph
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
    }

    /**
     * Adds fragment into Activity, set title to toolbar
     */
    fun addFragment(@IdRes id: Int, fragment: Fragment, toolbarTitle: String) {
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .replace(id, fragment)
                .disallowAddToBackStack()
                .commit()

        supportActionBar?.title = toolbarTitle
    }

    /**
     * Shows error message from string resources when error appears with ok button
     */
    override fun showError(id: Int) {
        val snackbar = Snackbar.make(findViewById(R.id.content), resourceManager.getString(id), Snackbar.LENGTH_INDEFINITE)
        snackbar.setAction(R.string.ok, { snackbar.dismiss() })
        snackbar.show()
    }

    /**
     * Shows custom error message when error appears with ok button
     */
    override fun showError(errorMessage: String) {
        val snackbar = Snackbar.make(findViewById(R.id.content), errorMessage, Snackbar.LENGTH_INDEFINITE)
        snackbar.setAction(R.string.ok, { snackbar.dismiss() })
        snackbar.show()
    }

    /**
     * Shows custom message
     */
    override fun showMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    /**
     * Shows message from string resources
     */
    override fun showMessage(id: Int) {
        Toast.makeText(this, resourceManager.getString(id), Toast.LENGTH_SHORT).show()
    }

    /**
     * Request user session manager for relogin and start welcome activity when token expires
     */
    override fun onTokenExpire() {
        userSessionManager.handleLogOut()
        intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }

}