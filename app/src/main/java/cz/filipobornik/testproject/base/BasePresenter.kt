package cz.filipobornik.testproject.base

import io.reactivex.disposables.CompositeDisposable

/**
 * Base Presenter class every presenter has to extend from
 */
abstract class BasePresenter<V : BaseMVPView> : BaseMVPPresenter<V> {

    /**
     * View attached to the presenter
     */
    var view: V? = null

    /**
     * For disposing all observes when view is detached
     */
    var compositeDisposable = CompositeDisposable()


    /**
     * Attach view to presenter
     */
    override fun attachView(view: V) {
        this.view = view
    }

    /**
     * Detach view from presenter, dispose composite disposable
     */
    override fun detachView() {
        this.view = null
        this.compositeDisposable.dispose()
    }

    /**
     * Check if view is attached to the presenter
     */
    override fun isViewAttached(): Boolean {
        return view != null
    }


}