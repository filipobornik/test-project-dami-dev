package cz.filipobornik.testproject.base

import android.content.Context
import android.support.v4.app.FragmentManager
import android.support.v7.app.AppCompatActivity
import com.google.firebase.analytics.FirebaseAnalytics
import cz.filipobornik.testproject.di.ActivityContext
import cz.filipobornik.testproject.di.PerActivity
import dagger.Module
import dagger.Provides
import javax.inject.Named

/**
 * Base Activity module provides dependencies for DI
 */
@Module
class BaseActivityModule {

    companion object {
        const val ACTIVITY_FRAGMENT_MANAGER = "BaseActivityModule.activityFragmentManager"
    }

    /**
     * Provides activity context
     */
    @Provides
    @ActivityContext
    @PerActivity
    fun activityContext(activity: AppCompatActivity): Context = activity

    /**
     * Provides fragment manager for base activity
     */
    @Provides
    @PerActivity
    @Named(ACTIVITY_FRAGMENT_MANAGER)
    fun provideActivityFragmentManager(activity: AppCompatActivity): FragmentManager = activity.supportFragmentManager


    @Provides
    @PerActivity
    fun provideFirebaseAnalytics(@ActivityContext context: Context): FirebaseAnalytics = FirebaseAnalytics.getInstance(context)
}