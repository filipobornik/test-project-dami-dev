package cz.filipobornik.testproject.base

/**
 * Base MVP Presenter any presenter has to implement
 */
interface BaseMVPPresenter<in V : BaseMVPView> {

    fun attachView(view: V)

    fun detachView()

    fun isViewAttached(): Boolean

}