package cz.filipobornik.testproject.base

import dagger.Module

/**
 * Base FFragment module provides dependencies for DI
 */
@Module
class BaseFragmentModule {

    companion object {
        /**
         * Specifies activity child fragment
         */
        const val FRAGMENT = "BaseFragmentModule.fragment"
    }

}