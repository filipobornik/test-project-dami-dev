package cz.filipobornik.testproject.base

import android.support.annotation.StringRes

/**
 * Base MVP View any view has to implement
 */
interface BaseMVPView {

    fun showError(@StringRes id: Int)

    fun showError(errorMessage: String)

    fun showMessage(message: String)

    fun showMessage(@StringRes id: Int)

    fun onTokenExpire()

}