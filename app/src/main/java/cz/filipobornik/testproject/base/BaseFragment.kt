package cz.filipobornik.testproject.base

import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.widget.Toast
import cz.filipobornik.testproject.R
import cz.filipobornik.testproject.di.ActivityContext
import cz.filipobornik.testproject.ui.welcomeActivity.WelcomeActivity
import cz.filipobornik.testproject.utils.UserSessionManager
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

/**
 * Base fragment every fragment has to extend from
 */
abstract class BaseFragment : Fragment(), BaseMVPView {

    @Inject
    @field:ActivityContext
    protected lateinit var activityContext: Context

    @Inject
    protected lateinit var resourceManager: Resources

    /**
     * Session manager take care about logged user session
     */
    @Inject
    lateinit var userSessionManager: UserSessionManager

    /**
     * Inject dependencies for fragment
     */
    override fun onAttach(context: Context?) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    /**
     * Shows error message from string resources when error appears
     */
    override fun showError(id: Int) {
        val snackbar = Snackbar.make(activity!!.findViewById(R.id.content), resourceManager.getString(id), Snackbar.LENGTH_INDEFINITE)
        snackbar.setAction(R.string.ok, { snackbar.dismiss() })
        snackbar.show()
    }

    /**
     * Shows custom error message when error appears
     */
    override fun showError(errorMessage: String) {
        val snackbar = Snackbar.make(activity!!.findViewById(R.id.content), errorMessage, Snackbar.LENGTH_INDEFINITE)
        snackbar.setAction(R.string.ok, { snackbar.dismiss() })
        snackbar.show()
    }

    /**
     * Shows custom message
     */
    override fun showMessage(message: String) {
        Toast.makeText(activityContext, message, Toast.LENGTH_SHORT).show()
    }

    /**
     * Shows message from string resources
     */
    override fun showMessage(id: Int) {
        Toast.makeText(activityContext, resourceManager.getString(id), Toast.LENGTH_SHORT).show()
    }

    /**
     * Request user for relogin when token expires
     */
    override fun onTokenExpire() {
        userSessionManager.handleLogOut()
        val intent = Intent(context, WelcomeActivity::class.java)
        startActivity(intent)
    }

}