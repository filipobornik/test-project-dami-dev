package cz.filipobornik.testproject.di.module

import android.app.Application
import android.content.Context
import cz.filipobornik.testproject.App
import cz.filipobornik.testproject.data.remote.NetModule
import cz.filipobornik.testproject.di.ApplicationContext
import cz.filipobornik.testproject.di.PerActivity
import cz.filipobornik.testproject.ui.addContactActivity.AddContactActivity
import cz.filipobornik.testproject.ui.addContactActivity.AddContactActivityModule
import cz.filipobornik.testproject.ui.contactDetailActivity.ContactDetailActivity
import cz.filipobornik.testproject.ui.contactDetailActivity.ContactDetailActivityModule
import cz.filipobornik.testproject.ui.main.MainActivity
import cz.filipobornik.testproject.ui.main.MainActivityModule
import cz.filipobornik.testproject.ui.registrationActivity.RegistrationActivity
import cz.filipobornik.testproject.ui.registrationActivity.RegistrationActivityModule
import cz.filipobornik.testproject.ui.editUserAccountActivity.EditUserAccountActivity
import cz.filipobornik.testproject.ui.editUserAccountActivity.EditUserAccountActivityModule
import cz.filipobornik.testproject.ui.userAccountDetailActivity.UserAccountDetailActivity
import cz.filipobornik.testproject.ui.userAccountDetailActivity.UserAccountDetailActivityModule
import cz.filipobornik.testproject.ui.welcomeActivity.WelcomeActivity
import cz.filipobornik.testproject.ui.welcomeActivity.WelcomeActivityModule
import dagger.Binds
import dagger.Module
import dagger.android.AndroidInjectionModule
import dagger.android.ContributesAndroidInjector
import javax.inject.Singleton

@Module(includes = [AndroidInjectionModule::class, NetModule::class, AppUtilsModule::class])
abstract class AppModule {

    @Binds
    @ApplicationContext
    @Singleton
    abstract fun provideContext(app: App): Context

    @Binds
    @Singleton
    abstract fun provideApplication(app: App): Application

    @PerActivity
    @ContributesAndroidInjector(modules = [MainActivityModule::class])
    abstract fun mainActivity(): MainActivity

    @PerActivity
    @ContributesAndroidInjector(modules = [WelcomeActivityModule::class])
    abstract fun welcomeActivity(): WelcomeActivity

    @PerActivity
    @ContributesAndroidInjector(modules = [ContactDetailActivityModule::class])
    abstract fun contactDetailActivity(): ContactDetailActivity

    @PerActivity
    @ContributesAndroidInjector(modules = [AddContactActivityModule::class])
    abstract fun addOrEditActivity(): AddContactActivity

    @PerActivity
    @ContributesAndroidInjector(modules = [RegistrationActivityModule::class])
    abstract fun registrationActivity(): RegistrationActivity

    @PerActivity
    @ContributesAndroidInjector(modules = [EditUserAccountActivityModule::class])
    abstract fun userAccountEditActivity(): EditUserAccountActivity

    @PerActivity
    @ContributesAndroidInjector(modules = [UserAccountDetailActivityModule::class])
    abstract fun userAccountDetailActivity(): UserAccountDetailActivity

}