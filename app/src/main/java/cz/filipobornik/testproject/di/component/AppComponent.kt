package cz.filipobornik.testproject.di.component

import cz.filipobornik.testproject.App
import cz.filipobornik.testproject.di.module.AppModule
import dagger.Component
import dagger.android.AndroidInjector
import javax.inject.Singleton

/**
 * AppComponent adding application into dagger dependency graph
 */
@Singleton
@Component(modules = [AppModule::class])
interface AppComponent : AndroidInjector<App> {

    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<App>()

}