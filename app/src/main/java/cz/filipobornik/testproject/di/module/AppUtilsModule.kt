package cz.filipobornik.testproject.di.module

import android.content.Context
import android.content.SharedPreferences
import android.content.res.Resources
import cz.filipobornik.testproject.App
import cz.filipobornik.testproject.R
import dagger.Module
import dagger.Provides
import io.realm.Realm
import javax.inject.Singleton

@Module
class AppUtilsModule {

    @Provides
    @Singleton
    fun provideResourceManager(app: App): Resources = app.resources

    @Provides
    @Singleton
    fun provideSharedPreferences(app: App, resourceManager: Resources): SharedPreferences = app.getSharedPreferences(resourceManager.getString(R.string.preference_file_key), Context.MODE_PRIVATE)

}