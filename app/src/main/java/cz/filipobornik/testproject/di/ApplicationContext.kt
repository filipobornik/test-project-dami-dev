package cz.filipobornik.testproject.di

import javax.inject.Qualifier

/**
 * Created by Filip Oborník (filipobornik@gmail.com) on 3/15/2018.
 */

@MustBeDocumented
@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class ApplicationContext