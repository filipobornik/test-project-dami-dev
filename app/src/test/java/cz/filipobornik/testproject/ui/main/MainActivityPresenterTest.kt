package cz.filipobornik.testproject.ui.main

import android.content.res.Resources
import cz.filipobornik.testproject.utils.UserSessionManager
import org.hamcrest.CoreMatchers.*
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnit
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class MainActivityPresenterTest {

    @Rule
    @JvmField
    val mockitoRule = MockitoJUnit.rule().silent()

    @Mock
    lateinit var view: MainActivityContract.View

    @Mock
    lateinit var userSessionManager: UserSessionManager

    @Mock
    lateinit var resources: Resources

    lateinit var presenter: MainActivityPresenter

    @Before
    fun setUp() {
        presenter = MainActivityPresenter(userSessionManager, resources)
        presenter.attachView(view)
    }

    @Test
    fun checkIsViewAttached_viewAttached_shouldReturnTrue() {
        presenter.attachView(view)
        assertThat(presenter.isViewAttached(), `is`(true))
    }

    @Test
    fun checkDetachingView_viewDetached_shouldReturnFalse() {
        presenter.detachView()
        assertThat(presenter.isViewAttached(), `is`(false))
    }

    @Test
    fun checkLogin_notLoggedUser_shouldRedirectToWelcomeActivity() {
        `when`(userSessionManager.isUserLoggedIn()).thenReturn(false)
        presenter.checkLogin()
        verify(view, atMost(1)).startWelcomeActivity()
    }

    @Test
    fun checkLogin_loggedUser_shouldNotStartWelcomeActivity() {
        `when`(userSessionManager.isUserLoggedIn()).thenReturn(true)
        presenter.checkLogin()
        verify(view, never()).startWelcomeActivity()
    }

    @Test
fun checkNavLogOutItemClick_userSessionManagerShouldHandleLogOut_shouldCallCheckLogin() {
        `when`(userSessionManager.isUserLoggedIn()).thenReturn(false)
        presenter.navLogOutItemClicked()
        verify(userSessionManager, atMost(1)).handleLogOut()
        verify(view, atMost(1)).startWelcomeActivity()
    }
}