package cz.filipobornik.testproject.data

import cz.filipobornik.testproject.data.model.Response
import cz.filipobornik.testproject.data.model.User
import cz.filipobornik.testproject.data.remote.DamiDevService
import cz.filipobornik.testproject.utils.ImageVersionSignature
import cz.filipobornik.testproject.utils.UserSessionManager
import cz.filipobornik.testproject.utils.schedulers.SchedulersProvider
import io.reactivex.Flowable
import io.reactivex.subscribers.TestSubscriber
import org.junit.Test
import org.junit.Before
import org.junit.Rule
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers.*
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnit
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class DataManagerTest {

    @Rule
    @JvmField
    val mockitoRule = MockitoJUnit.rule().silent()

    @Mock
    lateinit var damiDevService: DamiDevService

    @Mock
    lateinit var schedulersProvider: SchedulersProvider

    @Mock
    lateinit var userSessionManager: UserSessionManager

    private lateinit var dataManager: DataManager

    private lateinit var user: User
//
//    @Before
//    fun setUp() {
//        dataManager = DataManager(damiDevService, , userSessionManager)
//
//        val userId = 1
//        val userName = "Jan"
//        val userLastName = "Novak"
//        val userEmail = "jannovak@email.cz"
//        val userPhoto = "photo"
//        val userFIdString = "fidFb"
//        val userDescription = "some description"
//        val userPhone = "123456789"
//        val userToken = "a1b2c3d4e5"
//        val userImageVersionSignature = ImageVersionSignature()
//        user = User(userId, userName, userLastName, userEmail, userPhoto, userFIdString, userDescription, userPhone, userToken, userImageVersionSignature)
//    }
//
//    @Test
//    fun signIn_shouldReturnFlowable() {
//        `when`(damiDevService.login(anyString(), anyString())).thenReturn(Flowable.just(Response(0, "success", user)))
//        val testObserver: TestSubscriber<User?> = TestSubscriber()
//        dataManager.signIn(anyString(), anyString()).subscribe(testObserver)
//
//        testObserver.assertComplete()
//        testObserver.assertNoErrors()
//        testObserver.assertValueCount(1)
//    }

}